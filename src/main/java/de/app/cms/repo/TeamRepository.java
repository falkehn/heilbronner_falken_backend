package de.app.cms.repo;

import de.app.cms.model.team.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface TeamRepository extends JpaRepository<Team, Long> {
}
