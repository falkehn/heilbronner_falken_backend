package de.app.cms.repo;

import de.app.cms.model.push.PushDevice;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface PushDeviceRepository extends JpaRepository<PushDevice, Long> {

    List<PushDevice> findByPlatformType(PushDevice.PlatformType type);

    PushDevice findByDeviceToken(String deviceToken);

    /**
     * Searches PushDevice by deviceToken and platformType
     *
     * @param deviceToken desired Token
     * @param platformType desired PlatformType
     * @return PushDevice
     */
    PushDevice findByDeviceTokenAndPlatformType(String deviceToken, PushDevice.PlatformType platformType);
}
