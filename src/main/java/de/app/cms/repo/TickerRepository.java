package de.app.cms.repo;

import de.app.cms.model.ticker.Ticker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface TickerRepository extends JpaRepository<Ticker, Long> {

}

