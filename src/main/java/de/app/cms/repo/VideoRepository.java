package de.app.cms.repo;

import de.app.cms.model.video.Video;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface VideoRepository extends JpaRepository<Video, Long> {

    Video findByYoutubeVideoId(String youtubeVideoId);
}
