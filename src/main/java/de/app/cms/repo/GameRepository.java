package de.app.cms.repo;

import de.app.cms.model.game.Finals;
import de.app.cms.model.game.Game;
import de.app.cms.model.game.GameMode;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface GameRepository extends JpaRepository<Game, Long> {

    Game findByIsNextGame(boolean isNextGame);

    List<Game> findByGameMode(GameMode gameMode);
    List<Game> findByGameModeAndFinals(GameMode gameMode, Finals finals);
}
