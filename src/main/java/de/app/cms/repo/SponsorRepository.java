package de.app.cms.repo;


import de.app.cms.model.sponsor.Sponsor;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface SponsorRepository extends JpaRepository<Sponsor, Long> {
}
