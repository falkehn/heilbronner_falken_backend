package de.app.cms.repo;

import de.app.cms.model.image.BannerImage;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface BannerImageRepository extends JpaRepository<BannerImage, String> {
}
