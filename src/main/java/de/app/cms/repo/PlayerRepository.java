package de.app.cms.repo;

import de.app.cms.model.player.Player;
import de.app.cms.model.player.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PlayerRepository extends JpaRepository<Player, Long> {

    Player findByLastNameAndFirstName(String lastName, String firstName);

    Player findByLastNameAndFirstNameAndTeam(String lastName, String firstName, String team);

    Player findByNumberAndLastNameAndFirstName(int number, String lastName, String firstName);

    Player findByNumberAndLastNameAndFirstNameAndTeam(int number, String lastName, String firstName, String team);


    List<Player> findByTeam(String team);

    List<Player> findByPosition(Position position);

    List<Player> findByPositionAndTeam(Position position, String team);

    // koennen mehere sein wenn sie alle mit einer 0 am anfang einer saison gepflegt wurden
    List<Player> findByNumber(int number);

    @Query("SELECT p FROM player p WHERE goals=(SELECT MAX(goals) FROM player)")
    List<Player> findTopScorer();

    @Modifying
    @Query("delete from de.app.cms.model.player.Player p where p.team = :team")
    @Transactional
    void deleteByTeam(@Param("team")String team);
}
