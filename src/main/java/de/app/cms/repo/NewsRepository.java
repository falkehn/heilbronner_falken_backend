package de.app.cms.repo;

import de.app.cms.model.news.News;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface NewsRepository extends JpaRepository<News, Long> {

    List<News> findAllByOrderByDateDesc();
    News findByCustomId(String customId);
}
