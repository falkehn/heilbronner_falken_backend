package de.app.cms.repo;

import de.app.cms.model.push.TickerPushMessage;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface TickerPushMessageRepository extends JpaRepository<TickerPushMessage, Long> {
}
