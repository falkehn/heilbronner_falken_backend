package de.app.cms.controller;

import de.app.cms.controller.model.Message;
import de.app.cms.model.news.News;
import de.app.cms.push.PushMessageSender;
import de.app.cms.repo.NewsRepository;
import de.app.cms.repo.PushDeviceRepository;
import de.app.cms.utils.FalkenPageUtils;
import de.app.cms.utils.UrlPageUtils;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping("/link")
public class LinkController {

    @Autowired
    private NewsRepository mNewsRepository;
    @Autowired
    private PushMessageSender mPushMessageSender;

    private boolean mSaved;
    private boolean mError;

    @RequestMapping(method = RequestMethod.GET)
    public String getNewsPage(Message message, Model model) {
        model.addAttribute("success", mSaved);
        model.addAttribute("error", mError);
        mSaved = false;
        mError = false;
        return "link";
    }

    @RequestMapping(method=RequestMethod.POST)
    public String share(@Valid Message html, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "link";
        }

        final String link = html.getLink();
        News news;
        //check if hn-falken seite
        if(link.startsWith("http://www.heilbronner-falken.de/") || link.startsWith("http://heilbronner-falken.de/")
                || link.startsWith("heilbronner-falken.de/")) {
            news = FalkenPageUtils.getNewsForLink(link);
        } else {
            news = UrlPageUtils.newsForPageUrl(link);
        }

        if (news != null) {
            news.setPublic(true);
            if (mNewsRepository.save(news) != null) {
                mPushMessageSender.sendPush(news.getTitle(), news.getSecondaryTitle(), String.valueOf(news.getId()), news.getNewsType(),
                        news.getLink());
                mSaved = true;
            } else {
                mError = true;
            }
        } else {
            mError = true;
        }

        return "redirect:/link";

    }
}