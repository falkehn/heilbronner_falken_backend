package de.app.cms.controller;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import de.app.cms.model.push.PushDevice;
import de.app.cms.model.push.iOSPushDevice;
import de.app.cms.push.ios.IOSPushService;
import de.app.cms.repo.PushDeviceRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static javax.servlet.http.HttpServletResponse.SC_OK;

@RestController
//@RequestMapping("/FalkenService")
@RequestMapping("/rest")
public class PushDeviceRestController {

    final static Logger sLogger = LogManager.getLogger(PushDeviceRestController.class);

    @Autowired
    private PushDeviceRepository mRepository;

    @RequestMapping(value = "/registerV2", method = RequestMethod.POST)
    public void registerOrUpdateFirebaseDevice(@RequestParam(value = "regId") String deviceToken,
                                              HttpServletResponse servletResponse) {
        try {
            doRegisterOrUpdatePushDevice(deviceToken, PushDevice.PlatformType.FIREBASE);
            servletResponse.setStatus(SC_OK);
        } catch (IllegalArgumentException e) {
            // parsing error
            servletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            sLogger.error(e.getMessage(), e);
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public void registerOrUpdateAndroidDevice(@RequestParam(value = "regId") String deviceToken,
                                              HttpServletResponse servletResponse) {
        try {
            doRegisterOrUpdatePushDevice(deviceToken, PushDevice.PlatformType.ANDROID);
            servletResponse.setStatus(SC_OK);
        } catch (IllegalArgumentException e) {
            // parsing error
            servletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            sLogger.error(e.getMessage(), e);
        }
    }

    @ExceptionHandler(Exception.class)
    @RequestMapping(value = "/iosregister", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public void registerOrUpdateIosDevice(HttpServletRequest request,
                                          HttpServletResponse servletResponse,
                                          Exception exc) {
        if (exc != null) {
            exc.printStackTrace();
        }

        try {
            String registrationId = null;
            Map<String, String[]> params = request.getParameterMap();
            Iterator it = params.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                System.out.println("key:" + pair.getKey() + " value: " + pair.getValue());
                Gson gson = new Gson();
                iOSPushDevice pushService = gson.fromJson(pair.getKey().toString(), iOSPushDevice.class);
                registrationId = pushService.regId;
            }
            System.out.println("iosregister called: " + registrationId);

            doRegisterOrUpdatePushDevice(registrationId, PushDevice.PlatformType.IOS);
            servletResponse.setStatus(SC_OK);
        } catch (IllegalArgumentException e) {
            // parsing error
            servletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            sLogger.error(e.getMessage(), e);
        }
    }

    private void doRegisterOrUpdatePushDevice(String deviceToken, PushDevice.PlatformType type) {
        PushDevice pushDevice = mRepository.findByDeviceTokenAndPlatformType(deviceToken, type);
        if (pushDevice == null) {
            pushDevice = new PushDevice();
            pushDevice.deviceToken = deviceToken;
            pushDevice.platformType = type;
        }
        mRepository.save(pushDevice);
    }

    @RequestMapping(value = "/unregister", method = RequestMethod.POST)
    public void unregisterPushDevice(@RequestParam(value = "regId") String deviceToken,
                                     HttpServletResponse servletResponse) {

        if (deviceToken == null || deviceToken.length() == 0) {
            servletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        PushDevice device = mRepository.findByDeviceToken(deviceToken);
        if (device != null) {
            mRepository.delete(device);
        }
        servletResponse.setStatus(SC_OK);
    }
}
