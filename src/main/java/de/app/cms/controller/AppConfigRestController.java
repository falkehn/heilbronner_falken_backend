package de.app.cms.controller;


import de.app.cms.config.CMSConfig;
import de.app.cms.model.image.BannerImage;
import de.app.cms.repo.BannerImageRepository;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

@RestController
@RequestMapping("/rest")
public class AppConfigRestController {

    @Autowired
    private BannerImageRepository mBannerImageRepository;

    @RequestMapping(value="/newBanner")
    public String changeBanner(@RequestParam(value = "id", required = true) String id, HttpServletResponse servletResponse) {
        final List<BannerImage> allImages = mBannerImageRepository.findAll();
        for (BannerImage bannerImage : allImages) {
            bannerImage.active = false;
        }
        mBannerImageRepository.save(allImages);
        final BannerImage bannerImage = mBannerImageRepository.findOne(id);
        final String imgPath = CMSConfig.getInstance().get(CMSConfig.BANNER_IMG_PATH);
        try {
            byte[] bytes = downloadImage(bannerImage.url);
            BufferedOutputStream buffStream =
                    new BufferedOutputStream(new FileOutputStream(new File(imgPath)));
            buffStream.write(bytes);
            buffStream.close();
            bannerImage.active = true;
            mBannerImageRepository.save(bannerImage);
        } catch (IOException e) {
            e.printStackTrace();
        }

        servletResponse.setStatus(HttpServletResponse.SC_OK);
        return "Successfully changed Banner.";
    }

    private byte[] downloadImage(String url) throws IOException {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();
        int imageLength = (int)(entity.getContentLength());
        InputStream is = entity.getContent();

        byte[] imageBlob = new byte[imageLength];
        int bytesRead = 0;
        while (bytesRead < imageLength) {
            int n = is.read(imageBlob, bytesRead, imageLength - bytesRead);
            if (n <= 0)
                ; // do some error handling
            bytesRead += n;
        }

        return imageBlob;
    }
}
