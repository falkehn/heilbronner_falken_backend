package de.app.cms.controller;

import com.google.gson.Gson;
import de.app.cms.model.game.Game;
import de.app.cms.model.game.NextGame;
import de.app.cms.model.news.News;
import de.app.cms.model.news.NewsResponse;
import de.app.cms.model.news.NewsType;
import de.app.cms.push.PushMessageSender;
import de.app.cms.repo.GameRepository;
import de.app.cms.repo.NewsRepository;
import de.app.cms.utils.ImageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
//@RequestMapping("/FalkenService/rest")
@RequestMapping("/rest")
public class NewsRestController {

    @Autowired
    private GameRepository mGameRepository;

    @Autowired
    private NewsRepository mNewsRepository;

    @Autowired
    private PushMessageSender mPushMessageSender;

    @RequestMapping(value = "/publishForAndroid")
    public String publishForAndroid(@RequestParam(value = "id", required = true) long id) {
        final News news = mNewsRepository.findOne(id);
        news.setPublic(true);
        mPushMessageSender.sendFirebasePush(news.getTitle(), news.getSecondaryTitle(), String.valueOf(news.getId()),
                NewsType.Web, news.getLink());
        return "Successfully published News.";
    }

    @RequestMapping(value = "/publish")
    public String publish(@RequestParam(value = "id", required = true) long id) {
        final News news = mNewsRepository.findOne(id);
        news.setPublic(true);
        mPushMessageSender.sendPush(news.getTitle(), news.getSecondaryTitle(), String.valueOf(news.getId()), NewsType.Web, news.getLink());
        return "Successfully published News.";
    }

    @RequestMapping(value = "/nextGame", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getNextGame() {
        final Game game = mGameRepository.findByIsNextGame(true);
        final NextGame nextGame = new NextGame();
        nextGame.awayTeamImgUrl = ImageUtils.getGameImageName(game.team2);
        nextGame.homeTeamImgUrl = ImageUtils.getGameImageName(game.team1);
        nextGame.date = game.date;
        return nextGame.toString();
    }

    //@RequestMapping(value = "/NewsListV3", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/news", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getNewsWithNextGame(HttpServletResponse servletResponse) {
        List<News> news = mNewsRepository.findAllByOrderByDateDesc();
        List<News> firstFifty = new ArrayList<>();
        int i = 0;
        while (firstFifty.size() < 50) {
            final News newsEntry = news.get(i);
            if (newsEntry.isPublic() && !newsEntry.isOnlyFirebase()) {
                firstFifty.add(news.get(i));
            }
            i++;
        }
        Game game = mGameRepository.findByIsNextGame(true);
        if (game != null) {
            NewsResponse.NewsResponseV3.NextGame nextGame = new NewsResponse.NewsResponseV3.NextGame(game.team1, game.team2,
                game.date, game.homeTeamImgUrl, game.awayTeamImgUrl);
            return new NewsResponse.NewsResponseV3(nextGame, firstFifty).toString();
        }

        return new NewsResponse.NewsResponseV3(null, firstFifty).toString();
    }

    @RequestMapping(value = "/newsV2", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getNews(HttpServletResponse servletResponse) {
        List<News> news = mNewsRepository.findAllByOrderByDateDesc();
        List<News> firstFifty = new ArrayList<>();
        int i = 0;
        while (firstFifty.size() < 50) {
            final News newsEntry = news.get(i);
            if (newsEntry.isPublic()) {
                firstFifty.add(news.get(i));
            }
            i++;
        }
        servletResponse.setStatus(HttpServletResponse.SC_OK);
        Gson gson = new Gson();
        return gson.toJson(firstFifty);
    }
}
