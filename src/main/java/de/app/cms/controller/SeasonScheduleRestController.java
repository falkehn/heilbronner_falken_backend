package de.app.cms.controller;


import de.app.cms.model.game.*;
import de.app.cms.repo.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
//@RequestMapping("/FalkenService/rest")
@RequestMapping("/rest")
public class SeasonScheduleRestController {

    @Autowired
    private GameRepository mGameRepository;

    //@RequestMapping("SeasonSchedule")
    @RequestMapping("seasonschedule")
    public String getSeasonSchedule() {
        SeasonScheduleResponse.Season season = new SeasonScheduleResponse.Season();
        season.intermediateRound = mGameRepository.findByGameMode(GameMode.IntermediateRound);
        season.mainRound = mGameRepository.findByGameMode(GameMode.MainRound);
        season.playdowns = mGameRepository.findByGameMode(GameMode.Playdowns);
        season.preperation = mGameRepository.findByGameMode(GameMode.Preperation);

        List<Game> playOffs1 = mGameRepository.findByGameModeAndFinals(GameMode.Playoffs, Finals.Viertelfinale);
        List<Game> playOffs2 = mGameRepository.findByGameModeAndFinals(GameMode.Playoffs, Finals.Halbfinale);
        List<Game> playOffs3 = mGameRepository.findByGameModeAndFinals(GameMode.Playoffs, Finals.Finale);
        HashMap<Finals, List<Game>> playoffs = new HashMap<>();
        playoffs.put(Finals.Viertelfinale, playOffs1);
        playoffs.put(Finals.Halbfinale, playOffs2);
        playoffs.put(Finals.Finale, playOffs3);
        season.playoffs = playoffs;

        List<Game> cup1 = mGameRepository.findByGameModeAndFinals(GameMode.Cup, Finals.Runde_1);
        List<Game> cup2 = mGameRepository.findByGameModeAndFinals(GameMode.Cup, Finals.Achtelfinale);
        List<Game> cup3 = mGameRepository.findByGameModeAndFinals(GameMode.Cup, Finals.Viertelfinale);
        List<Game> cup4 = mGameRepository.findByGameModeAndFinals(GameMode.Cup, Finals.Halbfinale);
        List<Game> cup5 = mGameRepository.findByGameModeAndFinals(GameMode.Cup, Finals.Finale);
        HashMap<Finals, List<Game>> cup = new HashMap<>();
        cup.put(Finals.Runde_1, cup1);
        cup.put(Finals.Achtelfinale, cup2);
        cup.put(Finals.Viertelfinale, cup3);
        cup.put(Finals.Halbfinale, cup4);
        cup.put(Finals.Finale, cup5);
        season.cup = cup;

        List<SeasonScheduleResponse.Season> seasons = new ArrayList<>();
        seasons.add(season);

        SeasonScheduleResponse response = new SeasonScheduleResponse();
        response.seasons = seasons;
        return response.toString();
    }

    @RequestMapping("seasonscheduleV2")
    public String getSeasonScheduleV2() {
        Season season = new Season();
        final List<Game> intermidateRound = mGameRepository.findByGameMode(GameMode.IntermediateRound);
        if (intermidateRound != null && intermidateRound.size() > 0) {
            season.intermediateRound = intermidateRound;
        }

        final List<Game> mainRound = mGameRepository.findByGameMode(GameMode.MainRound);
        if (mainRound != null && mainRound.size() > 0) {
            season.mainRound = mGameRepository.findByGameMode(GameMode.MainRound);
        }

        final List<Game> playdowns = mGameRepository.findByGameMode(GameMode.Playdowns);
        if (playdowns != null && playdowns.size() > 0) {
            season.playdowns = playdowns;
        }

        final List<Game> preperation = mGameRepository.findByGameMode(GameMode.Preperation);
        if (preperation != null && preperation.size() > 0) {
            season.preperation = preperation;
        }

        List<Game> playOffs1 = mGameRepository.findByGameModeAndFinals(GameMode.Playoffs, Finals.Viertelfinale);
        List<Game> playOffs2 = mGameRepository.findByGameModeAndFinals(GameMode.Playoffs, Finals.Halbfinale);
        List<Game> playOffs3 = mGameRepository.findByGameModeAndFinals(GameMode.Playoffs, Finals.Finale);
        if (playOffs1 != null && playOffs1.size() > 0) {
            HashMap<Finals, List<Game>> playoffs = new HashMap<>();
            playoffs.put(Finals.Viertelfinale, playOffs1);

            if (playOffs2 != null && playOffs2.size() > 0) {
                playoffs.put(Finals.Halbfinale, playOffs2);

                if (playOffs3 != null && playOffs3.size() > 0) {
                    playoffs.put(Finals.Finale, playOffs3);
                }
            }
            season.playoffs = playoffs;
        }

        List<Game> cup1 = mGameRepository.findByGameModeAndFinals(GameMode.Cup, Finals.Runde_1);
        if (cup1 != null && cup1.size() > 0) {
            HashMap<Finals, List<Game>> cup = new HashMap<>();
            cup.put(Finals.Runde_1, cup1);

            List<Game> cup2 = mGameRepository.findByGameModeAndFinals(GameMode.Cup, Finals.Achtelfinale);
            if (cup2 != null && cup2.size() > 0) {
                cup.put(Finals.Achtelfinale, cup2);

                List<Game> cup3 = mGameRepository.findByGameModeAndFinals(GameMode.Cup, Finals.Viertelfinale);
                if (cup3 != null && cup3.size() > 0) {
                    cup.put(Finals.Viertelfinale, cup3);

                    List<Game> cup4 = mGameRepository.findByGameModeAndFinals(GameMode.Cup, Finals.Halbfinale);
                    if (cup4 != null && cup4.size() > 0) {
                        cup.put(Finals.Halbfinale, cup4);

                        List<Game> cup5 = mGameRepository.findByGameModeAndFinals(GameMode.Cup, Finals.Finale);
                        if (cup5 != null && cup5.size() > 0) {
                            cup.put(Finals.Finale, cup5);
                        }
                    }
                }
            }
            season.cup = cup;
        }

        List<Season> seasons = new ArrayList<>();
        seasons.add(season);

        return season.toString();
    }
}
