package de.app.cms.controller;

import de.app.cms.model.video.Video;
import de.app.cms.model.video.VideoResponse;
import de.app.cms.repo.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RestController
//@RequestMapping("/FalkenService/rest")
@RequestMapping("/rest")
public class VideoRestController {

    @Autowired
    private VideoRepository mVideRepository;

    //@RequestMapping("/VideoList")
    @RequestMapping("/videos")
    public String getVideos() {
        List<Video> videoList = mVideRepository.findAll();

        Collections.sort(videoList, (o1, o2) -> {
            if (o1.date.before(o2.date)) {
                return 1;
            } else {
                return -1;
            }
        });

        return new VideoResponse(videoList).toString();
    }

    @RequestMapping("/videosV2")
    public String getVideosV2() {
        List<Video> videoList = mVideRepository.findAll();

        Collections.sort(videoList, (o1, o2) -> {
            if (o1.date.before(o2.date)) {
                return 1;
            } else {
                return -1;
            }
        });

        return videoList.toString();
    }
}
