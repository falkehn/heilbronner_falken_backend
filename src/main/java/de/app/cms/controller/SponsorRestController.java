package de.app.cms.controller;

import de.app.cms.model.sponsor.Sponsor;
import de.app.cms.model.sponsor.SponsorMobile;
import de.app.cms.repo.SponsorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
//@RequestMapping("/FalkenService/rest")
@RequestMapping("/rest")
public class SponsorRestController {

    @Autowired
    private SponsorRepository mSponsorRepository;
    //@RequestMapping(value = "/SponsorService")
    @RequestMapping(value = "/sponsors")
    public List<SponsorMobile> getSponsors() {
        List<Sponsor> sponsors = mSponsorRepository.findAll();
        List<SponsorMobile> sponsorMobiles = new ArrayList<>();
        for (Sponsor sponsor : sponsors) {
            SponsorMobile sponsorMobile = new SponsorMobile();
            sponsorMobile.headerId = String.valueOf(sponsor.getRole().ordinal());
            sponsorMobile.imgName = sponsor.getImgurl();
            sponsorMobile.name = sponsor.getName();
            sponsorMobile.section = sponsor.getRole().getValue();
            sponsorMobile.url = sponsor.getWebsite();
            sponsorMobiles.add(sponsorMobile);
        }
        return sponsorMobiles;
    }
}
