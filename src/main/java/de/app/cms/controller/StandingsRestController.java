package de.app.cms.controller;


import de.app.cms.model.team.Team;
import de.app.cms.model.team.TeamsResponse;
import de.app.cms.repo.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
//@RequestMapping("/FalkenService/rest")
@RequestMapping("/rest")
public class StandingsRestController {

    @Autowired
    private TeamRepository mTeamRepository;

    //@RequestMapping("/Table")
    @RequestMapping("/standings")
    public String getStandings() {
        TeamsResponse response = new TeamsResponse();
        response.teams = mTeamRepository.findAll();
        return response.toString();
    }

    @RequestMapping("/standingsV2")
    public String getStandingsV2() {
        final List<Team> teams = mTeamRepository.findAll();
        for (int i = 0; i < teams.size(); i++) {
            teams.get(i).position = i+1;
        }
        return teams.toString();
    }
}
