package de.app.cms.controller;


import de.app.cms.config.CMSConfig;
import de.app.cms.controller.model.AppConfiguration;
import de.app.cms.model.image.BannerImage;
import de.app.cms.repo.BannerImageRepository;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/appconfig")
public class AppConfig {

    private boolean mSaved;

    @Autowired
    private BannerImageRepository mBannerImageRepository;

    @RequestMapping
    public String getAppConfigPage(ModelMap model) {
        model.addAttribute("success", mSaved);

        try {
            AppConfiguration configuration = AppConfiguration.read();
            model.addAttribute("appconfig", configuration);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mSaved = false;

        final long timeInMillis = new Date().getTime();
        model.addAttribute("img_src", "http://falkenapp.de/storage/appconfig/banner.png?time=" + timeInMillis);

        return "appconfig";
    }

    @RequestMapping(value="/changebanner", method= RequestMethod.POST)
    public String create(@RequestParam("uploadfile") MultipartFile uploadfile, HttpServletResponse response) {
        final String imgPath = CMSConfig.getInstance().get(CMSConfig.BANNER_IMG_PATH);
        try {
            byte[] bytes = uploadfile.getBytes();
            BufferedOutputStream buffStream =
                    new BufferedOutputStream(new FileOutputStream(new File(imgPath)));
            buffStream.write(bytes);
            buffStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);

        mSaved = true;

        return "redirect:/appconfig";
    }

    @RequestMapping(value =  "/saveAppConfig", method = RequestMethod.POST)
    public String saveAppConfig(AppConfiguration config, @RequestParam("uploadfile") MultipartFile uploadfile,
                                HttpServletResponse response) {
        if (uploadfile.getSize() > 0) {
            //save Banner
            final String imgPath = CMSConfig.getInstance().get(CMSConfig.BANNER_IMG_PATH);
            try {
                byte[] bytes = uploadfile.getBytes();
                BufferedOutputStream buffStream =
                        new BufferedOutputStream(new FileOutputStream(new File(imgPath)));
                buffStream.write(bytes);
                buffStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);

            mSaved = true;
        }

        try {
            AppConfiguration.save(config);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/appconfig";
    }
}
