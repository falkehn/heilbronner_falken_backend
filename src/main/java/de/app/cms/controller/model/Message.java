package de.app.cms.controller.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This Class is Part of the Project cms Copyright: Lidl E-Commerce
 * GmbH & Co. KG Stiftsbergstr. 1 74172 Neckarsulm
 *
 * @author tomaszadamczyk
 */
public class Message {

    private String title;

    @NotNull
    @Size(min = 5)
    private String link;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
