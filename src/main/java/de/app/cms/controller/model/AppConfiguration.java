package de.app.cms.controller.model;

import com.google.gson.Gson;
import de.app.cms.config.CMSConfig;
import de.app.cms.utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class AppConfiguration {

    public String ticketsUrl;

    public boolean runTickerNotifciatorEnabled;

    public boolean runLiveGameNotificatorEnabled;

    public String proxyUrl;

    public int proxyPort;

    public boolean isRunLiveGameNotificatorEnabled() {
        return runLiveGameNotificatorEnabled;
    }

    public void setRunLiveGameNotificatorEnabled(boolean runLiveGameNotificatorEnabled) {
        this.runLiveGameNotificatorEnabled = runLiveGameNotificatorEnabled;
    }

    public boolean getRunTickerNotifciatorEnabled() {
        return runTickerNotifciatorEnabled;
    }

    public void setRunTickerNotifciatorEnabled(boolean runTickerNotifciatorEnabled) {
        this.runTickerNotifciatorEnabled = runTickerNotifciatorEnabled;
    }

    public String getTicketsUrl() {
        return ticketsUrl;
    }

    public void setTicketsUrl(String ticketsUrl) {
        this.ticketsUrl = ticketsUrl;
    }

    public String getProxyUrl() {
        return proxyUrl;
    }

    public void setProxyUrl(String proxyUrl) {
        this.proxyUrl = proxyUrl;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public static AppConfiguration read() throws IOException {
        Gson gson = new Gson();
        String path = CMSConfig.getInstance().get(CMSConfig.APP_CONFIG_FILE_PATH);
        BufferedReader br = new BufferedReader(new FileReader(path));
        AppConfiguration configuration = gson.fromJson(br, AppConfiguration.class);
        if (configuration.getProxyUrl() == null) {
            configuration.setProxyUrl("81.169.145.246");
            configuration.setProxyPort(80);
        }
        return configuration;
    }

    public static void save(AppConfiguration appConfiguration) throws IOException {
        Gson gson = new Gson();
        String json = gson.toJson(appConfiguration);
        Utils.saveFile(CMSConfig.getInstance().get(CMSConfig.APP_CONFIG_FILE_PATH), json.getBytes());
    }
}
