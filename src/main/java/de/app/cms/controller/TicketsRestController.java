package de.app.cms.controller;

import de.app.cms.controller.model.AppConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/rest")
public class TicketsRestController {

    @RequestMapping("/tickets")
    public void getTicketsUrl(HttpServletResponse response) throws IOException {
        try {
            response.sendRedirect(AppConfiguration.read().getTicketsUrl());
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
        response.sendRedirect("http://www.heilbronner-falken.de");
    }
}
