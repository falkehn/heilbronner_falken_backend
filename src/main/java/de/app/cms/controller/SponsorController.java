package de.app.cms.controller;


import de.app.cms.config.CMSConfig;
import de.app.cms.controller.model.Message;
import de.app.cms.model.sponsor.Sponsor;
import de.app.cms.repo.SponsorRepository;
import de.app.cms.utils.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping("/sponsors")
public class SponsorController {

    @Autowired
    private SponsorRepository mSponsorRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String getSponsorPage(Model model, Sponsor sponsor, HttpServletResponse response) {
        model = getBaseModel(model);
        model.addAttribute("sponsor", sponsor);
        model.addAttribute("sponsortype", "new");
        response.setHeader("Pragma-directive:", "no-cache");
        response.setHeader("Cache-directive", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        response.setDateHeader("Expires", 0);

        return "sponsors";
    }

    @RequestMapping(value = "create", method= RequestMethod.POST)
    public String create(@Valid Sponsor sponsor, BindingResult bindingResult, Model model,@RequestParam("uploadfile") MultipartFile uploadfile, HttpServletResponse response) {
        if (bindingResult.hasErrors()) {
            model = getBaseModel(model);
            model.addAttribute("sponsor", sponsor);
            return "sponsors";
        }

        final String hashedName =  new Utils().toMD5(sponsor.getName()+sponsor.getWebsite()) + ".png";
        final String imgPath = CMSConfig.getInstance().get(CMSConfig.SPONSOR_IMG_PATH) + "/" +  hashedName;
        try {
            byte[] bytes = uploadfile.getBytes();
            BufferedOutputStream buffStream =
                    new BufferedOutputStream(new FileOutputStream(new File(imgPath)));
            buffStream.write(bytes);
            buffStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        sponsor.setFilePath(imgPath);
        sponsor.setImgurl("http://falkenapp.de/storage/sponsors/" + hashedName);

        mSponsorRepository.save(sponsor);
        response.setHeader("Pragma-directive:", "no-cache");
        response.setHeader("Cache-directive", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        response.setDateHeader("Expires", 0);

        return "redirect:/sponsors";
    }

    @RequestMapping(value="/edit")
    public String edit(Long id, ModelMap model) {
        Pageable pageRequest = new PageRequest(0, 100);
        Page<Sponsor> sponsors = mSponsorRepository.findAll(pageRequest);
        model.addAttribute("sponsors", sponsors);
        model.addAttribute("sponsor", mSponsorRepository.findOne(id));
        model.addAttribute("sponsortype", "update");
        return "sponsors";
    }

    @RequestMapping(value="/delete")
    public String delete(Long id) {
        Sponsor sponsor = mSponsorRepository.findOne(id);
        File file = new File(sponsor.getFilePath());
        file.delete();
        mSponsorRepository.delete(sponsor);
        return "redirect:/sponsors";
    }

    private Model getBaseModel(Model model) {
        Pageable pageRequest = new PageRequest(0, 100);
        Page<Sponsor> sponsors = mSponsorRepository.findAll(pageRequest);
        model.addAttribute("sponsors", sponsors);
        return model;
    }
}