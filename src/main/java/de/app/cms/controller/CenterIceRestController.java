package de.app.cms.controller;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.app.cms.utils.DEL2Stats;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/rest")
public class CenterIceRestController {


    @RequestMapping("/centerice")
    public String getVideos() throws IOException {
        Document document = DEL2Stats.getDocument("http://www.del-2.org/live/");
        Elements elements = document.getElementsByAttribute("src");
        for (Element element : elements) {
            String src = element.attr("src");
            if (!src.startsWith("http://")) {
                element.attr("src", "http://www.del-2.org" + src);
            }
        }

        elements = document.getElementsByAttribute("href");
        for (Element element : elements) {
            String src = element.attr("href");
            if (!src.startsWith("http://")) {
                element.attr("href", "http://www.del-2.org" + src);
            }
        }

        Elements navigation = document.select("#mobileheader");
        navigation.attr("style", "display: none;");

        Elements commercial = document.select(".naMediaAd_SUPERBANNER");
        commercial.attr("style", "display: none;");

        Elements wrongMargin = document.select("#ip_content_wrapper");
        wrongMargin.attr("style", "margin-top: 0px;");

        String html = document.html();
        Map<String, String> map = new HashMap<>();
        map.put("val", html);
        return new Gson().toJson(map);
    }
}
