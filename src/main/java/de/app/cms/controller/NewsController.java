package de.app.cms.controller;

import de.app.cms.config.CMSConfig;
import de.app.cms.controller.model.Message;
import de.app.cms.model.news.News;
import de.app.cms.model.news.NewsType;
import de.app.cms.push.PushMessageSender;
import de.app.cms.repo.NewsRepository;
import de.app.cms.utils.HtmlBuilder;
import de.app.cms.utils.Utils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static de.app.cms.utils.HtmlBuilder.*;

@Controller
@RequestMapping("/news")
public class NewsController {

    @Autowired
    private PushMessageSender mPushMessageSender;

    @Autowired
    private NewsRepository mNewsRepository;

    private boolean mSaved;

    @RequestMapping(method = RequestMethod.GET)
    public String getNewsPage(Message message, Model model) {
        model.addAttribute("success", mSaved);
        mSaved = false;

        return "news";
    }

    @RequestMapping(value="/postnews", method = RequestMethod.POST)
    public String share(@Valid Message html, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "news";
        }
        // generate full html page
        final String title = html.getTitle();
        News news = new News();
        news.setTitle(title);
        news.setDate(Calendar.getInstance().getTime());
        news.setNewsType(NewsType.Web);
        // to get id
        news = mNewsRepository.save(news);
        // create html + link
        final String fileName = news.getId() + ".html";
        final String filePath = CMSConfig.getInstance().get(CMSConfig.NEWS_HTML_PATH) + "/" + fileName;

        final String htmlPage = generateHtml(title, html.getLink());
        try {
            Utils.saveFile(filePath, htmlPage.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        final String link = Utils.shortenUrl("http://falkenapp.de/storage/news/" + fileName);
        news.setLink(link);

        // if its containts a img, we use it as thumbnail
        Elements possibleImages = Jsoup.parse(htmlPage).select("img");
        if (possibleImages.size() > 0) {
            String imgLink = possibleImages.get(0).absUrl("src");
            news.setImgUrl(imgLink);
        } else {
            news.setImgUrl("");
        }

        String secondaryTitle = Jsoup.parse(html.getLink()).text().trim();
        final int maxLength = 150;
        if (Math.min(secondaryTitle.length(), maxLength) == maxLength) {
            secondaryTitle = secondaryTitle.substring(0, Math.min(secondaryTitle.length(), maxLength)) + "...";
        }
        news.setSecondaryTitle(secondaryTitle);
        news.setPublic(true);

        if (mNewsRepository.save(news) != null) {
            mPushMessageSender.sendPush(title, secondaryTitle, String.valueOf(news.getId()), NewsType.Web, link);
        }

        mSaved = true;
        return "redirect:/news";
    }

    public String generateHtml(String title, String innerHtml) {
        // remove unneccessary img attributes
        Document doc = Jsoup.parse(innerHtml);
        Elements elements = doc.select("img");
        for (Element element : elements) {
            final String url = element.absUrl("src");
            Attributes attributes = element.attributes();
            for (Attribute attribute : attributes) {
                element.removeAttr(attribute.getKey());
            }
            element.attr("src", url);
        }
        innerHtml = doc.html();

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        String time = sdf.format(Calendar.getInstance().getTime());
        HtmlBuilder htmlBuilder = new HtmlBuilder();
        htmlBuilder.reset();

        htmlBuilder.addText(time + " Uhr", COLOR_BLUE, FONT_NORMAL, 18, ALIGN_LEFT);
        htmlBuilder.addText(title, COLOR_RED, FONT_BOLD, 20, ALIGN_LEFT);
        htmlBuilder.addHtml(innerHtml);

        Utils utils = new Utils();
        final String htmlSkeleton = utils.getHtmlSkeleton("news_skeleton.html");
        return htmlSkeleton.replace("INSERT_HTML_HERE", htmlBuilder.renderBody());
    }
}
