package de.app.cms.controller;


import de.app.cms.model.player.Player;
import de.app.cms.model.player.PlayerResponse;
import de.app.cms.model.player.PlayerV2;
import de.app.cms.model.player.Position;
import de.app.cms.repo.PlayerRepository;
import de.app.cms.utils.DEL2Stats;
import de.app.cms.utils.NameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
//@RequestMapping("/FalkenService/rest")
@RequestMapping("/rest")
public class PlayerRestController {

    @Autowired
    private PlayerRepository mPlayerRepository;

    //@RequestMapping("/Team")
    @RequestMapping("/team")
    public String getTeam() {
        List<Player> goalies = mPlayerRepository.findByPositionAndTeam(Position.Goalie, "Heilbronner Falken");
        List<Player> defenders = mPlayerRepository.findByPositionAndTeam(Position.Verteidiger, "Heilbronner Falken");
        List<Player> forcers = mPlayerRepository.findByPositionAndTeam(Position.Stürmer, "Heilbronner Falken");
        List<Player> trainers = mPlayerRepository.findByPositionAndTeam(Position.Trainer, "Heilbronner Falken");

        PlayerResponse.PlayerList playerList = new PlayerResponse.PlayerList(goalies, defenders, forcers, trainers);
        List<PlayerResponse.PlayerList> playerLists = new ArrayList<>();
        playerLists.add(playerList);
        PlayerResponse playerResponse = new PlayerResponse(playerLists);
        return playerResponse.toString();
    }

    @RequestMapping("/teamV2")
    public String getTeamV2() {
        List<Player> players = new ArrayList<>();
        players.addAll(mPlayerRepository.findByPositionAndTeam(Position.Goalie, "Heilbronner Falken"));
        players.addAll(mPlayerRepository.findByPositionAndTeam(Position.Verteidiger, "Heilbronner Falken"));
        players.addAll(mPlayerRepository.findByPositionAndTeam(Position.Stürmer, "Heilbronner Falken"));
        players.addAll(mPlayerRepository.findByPositionAndTeam(Position.Trainer, "Heilbronner Falken"));

        List<PlayerV2> playerLists = new ArrayList<>();
        for (Player player : players) {
            playerLists.add(PlayerV2.parse(player));
        }

        return playerLists.toString();
    }

    @RequestMapping(value = "/player", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public String getPlayer(@RequestParam(value = "firstname") String firstname,
                            @RequestParam(value = "lastname") String lastname,
                            @RequestParam(value = "number") int number) {
        return mPlayerRepository.findByNumberAndLastNameAndFirstName(number, lastname, firstname).toString();
    }

    @RequestMapping(value = "/playerByName", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public String getPlayer(@RequestParam(value = "name") String name) {
        String[] names = name.split(" ");
        return mPlayerRepository.findByLastNameAndFirstName(names[1], names[0]).toString();
    }

    @RequestMapping(value = "/playerByNameAndTeam", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public String getPlayerByTeam(@RequestParam(value = "name") String name, @RequestParam("team") String team) {
        String[] names = name.split(" ");
        team = NameUtils.tickerToDel2Mapper(team);
        return mPlayerRepository.findByLastNameAndFirstNameAndTeam(names[1], names[0], team).toString();
    }

    @RequestMapping(value = "/playerByNameAndTeamV2", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public String getPlayerByTeamV2(@RequestParam(value = "name") String name, @RequestParam("team") String team) {
        String[] names = name.split(" ");
        team = NameUtils.tickerToDel2Mapper(team);
        return PlayerV2.parse(mPlayerRepository.findByLastNameAndFirstNameAndTeam(names[1], names[0], team)).toString();
    }

    @RequestMapping(value = "/playerV2", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public String getPlayerByNameAndNumber(@RequestParam(value = "name") String name, @RequestParam("number") String number) {
        String[] names = name.split(" ");
        return PlayerV2.parse(mPlayerRepository.findByNumberAndLastNameAndFirstName(Integer.valueOf(number), names[1], names[0])).toString();
    }
}