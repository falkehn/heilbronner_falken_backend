package de.app.cms.config;

import de.app.cms.controller.model.AppConfiguration;
import org.springframework.context.ApplicationContext;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class CMSConfig {

    public static final String SPONSOR_IMG_PATH = "sponsor.img.path";
    public static final String BANNER_IMG_PATH = "banner.img.path";
    public static final String NEWS_HTML_PATH = "news.html.path";
    public static final String IOS_CERT_PATH = "ios.cert.path";
    public static final String TEAM_PATH = "team.path";
    public static final String APP_CONFIG_FILE_PATH = "app.config.file.path";

    private Map<String, String> mConfig;

    private static CMSConfig sConfig;

    public static CMSConfig getInstance() {
        if (sConfig == null) {
            sConfig = new CMSConfig();
        }
        return sConfig;
    }

    public void init(ApplicationContext context) throws IOException {
        mConfig = new HashMap<>();

        final String basePath = context.getResource("/").getFile().getAbsolutePath();
        final String toRemove = basePath.substring(basePath.lastIndexOf('/') + 1);
        String tomcatPath = basePath.replace(toRemove, "") + "/ROOT";
        createDirIfNotExists(tomcatPath);
        tomcatPath = tomcatPath + "/storage";
        createDirIfNotExists(tomcatPath);

        final String sponsorPath = tomcatPath + "/sponsors";
        mConfig.put(SPONSOR_IMG_PATH, sponsorPath);
        createDirIfNotExists(sponsorPath);

        final String bannerPath = tomcatPath + "/appconfig";
        mConfig.put(BANNER_IMG_PATH, bannerPath + "/banner.png");
        createDirIfNotExists(bannerPath);

        final String appConfigFilePath = tomcatPath + "/appconfig/appconf.json";
        mConfig.put(APP_CONFIG_FILE_PATH, appConfigFilePath);
        File appConfFile = new File(appConfigFilePath);
        if (!appConfFile.exists()) {
            appConfFile.createNewFile();
            AppConfiguration.save(new AppConfiguration());
        }

        final String newsPath = tomcatPath + "/news";
        mConfig.put(NEWS_HTML_PATH, newsPath);
        createDirIfNotExists(newsPath);

        final String teamPath = tomcatPath + "/team";
        mConfig.put(TEAM_PATH, teamPath);
        createDirIfNotExists(teamPath);

        final File certFile = context.getResource("classpath:files/push_cert.p12").getFile();
        final String certPath = tomcatPath + "/appconfig/push_cert.p12";
        copyFileUsingStream(certFile, new File(certPath));

        final File signingRequestCert = context.getResource("classpath:files/CertificateSigningRequest.certSigningRequest").getFile();
        final String signingRequestCertPath = tomcatPath + "/appconfig/CertificateSigningRequest.certSigningRequest";
        copyFileUsingStream(signingRequestCert, new File(signingRequestCertPath));

        final File mobileProvisionCert = context.getResource("classpath:files/HeilbronnerFalken_Push_Dist.mobileprovision").getFile();
        final String mobileProvisionCertPath = tomcatPath + "/appconfig/HeilbronnerFalken_Push_Dist.mobileprovision";
        copyFileUsingStream(mobileProvisionCert, new File(mobileProvisionCertPath));

        mConfig.put(IOS_CERT_PATH, certPath);
    }

    public String get(String key) {
        return mConfig.get(key);
    }

    private void createDirIfNotExists(String path) {
        final File file = new File(path);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    private void copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }
}
