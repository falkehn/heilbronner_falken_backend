package de.app.cms.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.yaml.DefaultProfileDocumentMatcher;
import org.springframework.boot.yaml.SpringProfileDocumentMatcher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("de.app.cms")
public class SpringDataConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private ApplicationContext mAppContext;

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        // tell JPA which model entities we have
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan(new String[] { "de.app.cms" });

        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        final Properties properties = getProperties();
        em.setJpaProperties(properties);

        return em;
    }

    @Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        final Properties properties = getProperties();
        dataSource.setDriverClassName(properties.getProperty("jdbc.driverClassName"));
        dataSource.setUrl(properties.getProperty("jdbc.url"));
        dataSource.setUsername(properties.getProperty("jdbc.user"));
        dataSource.setPassword(properties.getProperty("jdbc.pass"));

        try {
            final ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
            rdp.addScript(new ClassPathResource("quartz.sql"));
            rdp.populate(dataSource.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            CMSConfig.getInstance().init(mAppContext);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

        return transactionManager;
    }

    /**
     * Returns the Properties from the resources/persistence_config.yml file
     * @return
     */
    public Properties getProperties() {
        // find out which profile has been selected
        Environment environment = mAppContext.getEnvironment();
        final String[] activeProfiles = environment.getActiveProfiles();
        SpringProfileDocumentMatcher documentMatcher = new SpringProfileDocumentMatcher();
        // if not active profile has been set, we use the default profile
        if (activeProfiles.length == 0) {
            documentMatcher.addActiveProfiles("default");
        } else {
            // otherwise set the right profile configuration
            for (int i = 0; i < activeProfiles.length; i++) {
                documentMatcher.addActiveProfiles(activeProfiles[i]);
            }
        }
        YamlPropertiesFactoryBean yamlPropertiesBuilder = new YamlPropertiesFactoryBean();
        yamlPropertiesBuilder.setDocumentMatchers(new DefaultProfileDocumentMatcher(),
                documentMatcher);
        Resource resource = mAppContext.getResource("classpath:persistence_config.yml");
        yamlPropertiesBuilder.setResources(resource);
        yamlPropertiesBuilder.afterPropertiesSet();
        System.out.println("JDBCURL: " + yamlPropertiesBuilder.getObject().get("jdbc.url"));
        return yamlPropertiesBuilder.getObject();
    }
}