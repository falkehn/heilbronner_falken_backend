package de.app.cms.holema;

import com.google.gson.annotations.SerializedName;

public class HolemaPlayer {

    @SerializedName("@id")
    public String id;
    @SerializedName("@eliteprospectsid")
    public String eliteprospectsid;
    @SerializedName("firstname")
    public String firstname;
    @SerializedName("lastname")
    public String lastname;
    @SerializedName("jersey")
    public int number;
    @SerializedName("position")
    public String position;
    @SerializedName("games")
    public int games;
    @SerializedName("goals")
    public int goals;
    @SerializedName("assists")
    public int assists;
    @SerializedName("points")
    public int points;
    @SerializedName("plusminus")
    public int plusMinus;
    @SerializedName("image")
    public Image image;
    @SerializedName("nationality")
    public String nationality;
    @SerializedName("shoots")
    public String hand;
    @SerializedName("birthday")
    public String birthday;
    @SerializedName("height")
    public int height;
    @SerializedName("weight")
    public int weight;
    @SerializedName("penalties")
    public int penalties;

    public class Image {
        @SerializedName("image_200")
        public String src;
    }
}
