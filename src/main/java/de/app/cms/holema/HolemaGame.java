package de.app.cms.holema;

import com.google.gson.annotations.SerializedName;

public class HolemaGame {

    @SerializedName("@id")
    public int id;
    @SerializedName("gamedate")
    public String gamedate;
    @SerializedName("gametime")
    public String gametime;
    @SerializedName("spectators")
    public int visitors;
    @SerializedName("periodscore")
    public String periodscore;
    @SerializedName("homescore")
    public String homescore;
    @SerializedName("awayscore")
    public String awayscore;
    @SerializedName("resulttype")
    public String resulttype;
    @SerializedName("hometeam")
    public HolemaTeam hometeam;
    @SerializedName("awayteam")
    public HolemaTeam awayteam;
}
