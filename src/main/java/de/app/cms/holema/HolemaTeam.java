package de.app.cms.holema;

import com.google.gson.annotations.SerializedName;

public class HolemaTeam {

    @SerializedName("@id")
    public int id;
    @SerializedName("name")
    public String name;
    @SerializedName("games")
    public int games;
    @SerializedName("points")
    public int points;
    @SerializedName("goalsfor")
    public int goalsfor;
    @SerializedName("goalsagainst")
    public int goalsagainst;
    @SerializedName("penalties")
    public int penalties;
    @SerializedName("shortname")
    public String shortname;
}
