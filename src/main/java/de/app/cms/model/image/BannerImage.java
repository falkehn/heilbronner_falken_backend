package de.app.cms.model.image;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "banner_img")
public class BannerImage {

    @Id
    public String id;
    public String url;
    public boolean active;
}
