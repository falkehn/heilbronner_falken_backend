package de.app.cms.model.sponsor;

public enum Role {

    Haupt("Hauptsponsor")
    , Premium("Premium Partner")
    , BasisPartner("Basis Partner")
    , Fitness("Fitness- u. Gesundheitspartner")
    , Medien("Medienpartner")
    , TeamAustatter("Team Ausstatter")
    , Ticker("Falkenticker Sponsor");

    public String value;

    private Role(String value) {
        this.value = value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
