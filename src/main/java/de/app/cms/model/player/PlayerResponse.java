package de.app.cms.model.player;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlayerResponse {

    @SerializedName("team")
    public List<PlayerList> playerLists;

    public PlayerResponse(List<PlayerList> playerLists) {
        this.playerLists = playerLists;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        if (f.getName().equals("player")) {
                            return true;
                        }
                        return false;
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create()
                .toJson(this);
    }

    public static class PlayerList {

        @SerializedName("Goalie")
        public List<Player> goalies;
        @SerializedName("Verteidiger")
        public List<Player> defenders;
        @SerializedName("Stürmer")
        public List<Player> forcer;
        @SerializedName("Trainer")
        public List<Player> trainer;

        public PlayerList(List<Player> goalies, List<Player> defenders, List<Player> forcer, List<Player> trainer) {
            this.goalies = goalies;
            this.defenders = defenders;
            this.forcer = forcer;
            this.trainer = trainer;
        }
    }
}