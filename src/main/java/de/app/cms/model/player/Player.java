package de.app.cms.model.player;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "player")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    public String team;
    public String lastName;
    public String firstName;
    public Position position;
    public int number;
    public Nation nation;
    public String nationImgUrl;
    public Date birthday;
    public int size;
    public int weight;
    public String hand;
    public String falkeSince;
    public String largeImageUrl;
    public int goals;
    public int assists;
    public int scorerpoints;
    @SerializedName("games_played")
    public int gamesPlayed;
    @SerializedName("penalty_minutes")
    public int penaltyMinutes;
    public int plusMinus;
    public String imageUrl;
    public String link;
    public boolean isTopScorer;
    public boolean isFoeli;

    @Override
    public String toString() {
        return new GsonBuilder()
                .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        if (f.getName().equals("player")) {
                            return true;
                        }
                        return false;
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create()
                .toJson(this);
    }
}
