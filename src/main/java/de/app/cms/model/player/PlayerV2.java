package de.app.cms.model.player;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import de.app.cms.utils.NationUtils;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PlayerV2 {

    public String name;
    public String position;
    public int number;
    public String nation;
    public String birthday;
    public String sizeAndWeight;
    public String falkeSince;
    public String smallImage;
    public String largeImage;
    public int goals;
    public int assists;
    public int scorerpoints;
    public int plusMinus;
    @SerializedName("games_played")
    public int gamesPlayed;
    @SerializedName("penalty_minutes")
    public int penaltyMinutes;
    public String link;
    public boolean isTopScorer;
    public boolean isFoeli;

    @Override
    public String toString() {
        return new GsonBuilder()
                .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        if (f.getName().equals("player")) {
                            return true;
                        }
                        return false;
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create()
                .toJson(this);
    }

    public static PlayerV2 parse(final Player player) {
        PlayerV2 playerV2 = new PlayerV2();
        playerV2.assists = player.assists;
        final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy");
        playerV2.birthday = sdf.format(player.birthday);
        playerV2.falkeSince = player.falkeSince;
        playerV2.gamesPlayed = player.gamesPlayed;
        playerV2.goals = player.goals;
        playerV2.isFoeli = player.isFoeli;
        playerV2.isTopScorer = player.isTopScorer;
        playerV2.largeImage = player.largeImageUrl;
        playerV2.scorerpoints = player.scorerpoints;
        playerV2.smallImage = player.imageUrl;
        playerV2.link = player.link;
        playerV2.nation = NationUtils.getName(player.nation);
        playerV2.name = player.firstName + " " + player.lastName;
        playerV2.sizeAndWeight = "Gr: " + player.size + " cm, Gw: " + player.weight + " Kg";
        playerV2.penaltyMinutes = player.penaltyMinutes;
        playerV2.number = player.number;
        playerV2.position = player.position.toString();
        playerV2.plusMinus = player.plusMinus;
        return playerV2;
    }
}
