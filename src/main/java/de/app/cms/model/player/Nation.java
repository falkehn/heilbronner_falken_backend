package de.app.cms.model.player;

public enum Nation {
    German,
    Canada,
    USA,
    Slowakai,
    Russian,
    Ukraine,
    Sweden,
    Poland,
    Czech,
    Latvia,
    WhiteRussia,
    Wales,
    France,
    Finnland
}
