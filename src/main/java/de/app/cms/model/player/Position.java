package de.app.cms.model.player;

public enum Position {
    Goalie,
    Verteidiger,
    Stürmer,
    Trainer
}
