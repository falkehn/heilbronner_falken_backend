package de.app.cms.model.video;

import com.google.gson.GsonBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity(name = "video")
public class Video {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    public String youtubeVideoId;
    public String imgUrl;
    public String title;
    public String androidUrl;
    public String iosUrl;
    public String secondaryTitle;
    public Date date;
    public long	duration;

    @Override
    public String toString() {
        return new GsonBuilder()
                .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                .create()
                .toJson(this);
    }
}
