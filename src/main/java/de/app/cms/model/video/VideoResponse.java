package de.app.cms.model.video;

import com.google.gson.GsonBuilder;

import java.util.List;

public class VideoResponse {

    public List<Video> videoItems;

    public VideoResponse(List<Video> videoList) {
        this.videoItems = videoList;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                .create()
                .toJson(this);
    }
}
