package de.app.cms.model.team;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Home on 11.07.2015.
 */
public class TeamsResponse {

    @SerializedName("teams")
    public List<Team> teams;

    @Override
    public String toString() {
        return new GsonBuilder()
                .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                .create()
                .toJson(this);
    }

}
