package de.app.cms.model.team;


import com.google.gson.Gson;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "team")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;
    public String name;
    public int points;
    public int gamesCount;
    public String goals;
    public String localImageName;
    public int goalsShot;
    public int goalsGet;
    public int position;
    public String shortname;
    
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
