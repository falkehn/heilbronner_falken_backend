package de.app.cms.model.push;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.persistence.*;

@Entity(name = "pushdevice")
public class PushDevice {

    /**
     * Enums for the platformType of the PushDevice
     */
    public enum PlatformType {
        IOS, ANDROID, FIREBASE
    }

    /**
     * Id of the PushDevice which is also the primary key in the database
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;

    /**
     * Token which is generated from the device and was sent to the server
     */
    public String deviceToken;

    /**
     * PlatformType of the PushDevice
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "pushDeviceType")
    public PlatformType platformType;

    @Override
    public String toString() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        return gson.toJson(this);
    }

    public boolean isValid() {
        return platformType != null && deviceToken != null;
    }
}
