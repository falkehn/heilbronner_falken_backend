package de.app.cms.model.push;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "tickerpushmessage")
public class TickerPushMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    public int matchId;
    public String title;
    public String message;

    public TickerPushMessage() {
    }

    public TickerPushMessage(final int matchId, final String title, final String message) {
        this.matchId = matchId;
        this.title = title;
        this.message = message;
    }
}
