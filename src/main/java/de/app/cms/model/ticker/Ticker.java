package de.app.cms.model.ticker;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "ticker")
public class Ticker {

    @Id
    public String current_match_id;
    public long last_update;
}
