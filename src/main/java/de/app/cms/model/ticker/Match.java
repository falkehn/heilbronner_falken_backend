package de.app.cms.model.ticker;

import javax.persistence.GenerationType;
import java.util.List;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;

public class Match {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public String id;
    public String title;
    public String date;
    public String location;
    public String type;
    public String progress;
    public String score_1_period;
    public String score_2_period;
    public String score_3_period;
    public String score_overtime;
    public String score_penalty;
    public String score_total;
    public String online;
    public String archive;
    public String team_home_name;
    public String team_home_url;
    public String team_home_logo;
    public String team_home_penalty;
    public String team_visitor_name;
    public String team_visitor_url;
    public String team_visitor_logo;
    public String team_visitor_penalty;
    public List<String> all_situation_ids;
    public List<String> all_goal_ids;
    public List<String> all_penalty_ids;
}
