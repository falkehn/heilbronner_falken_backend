package de.app.cms.model.game;

public enum GameMode {
    Preperation, 	//Vorrunde
    MainRound, 		//Hauptrunde
    Playoffs,
    Cup,			//Pokal
    IntermediateRound,
    Playdowns
}
