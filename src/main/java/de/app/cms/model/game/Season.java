package de.app.cms.model.game;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;

public class Season {

    @SerializedName("Preperation")
    public List<Game> preperation;
    @SerializedName("MainRound")
    public List<Game> mainRound;
    @SerializedName("IntermediateRound")
    public List<Game> intermediateRound;
    @SerializedName("Playdowns")
    public List<Game> playdowns;
    @SerializedName("Playoffs")
    public HashMap<Finals, List<Game>> playoffs;
    @SerializedName("Cup")
    public HashMap<Finals, List<Game>> cup;
    @Override
    public String toString() {
        return new GsonBuilder()
                .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                .create()
                .toJson(this);
    }
}
