package de.app.cms.model.game;

import com.google.gson.GsonBuilder;

import java.util.Date;

/**
 * This Class is Part of the Project cms Copyright: Lidl E-Commerce
 * GmbH & Co. KG Stiftsbergstr. 1 74172 Neckarsulm
 *
 * @author tomaszadamczyk
 */
public class NextGame {

    public String imgUrl;
    public String homeTeamImgUrl;
    public String awayTeamImgUrl;
    public Date date;

    @Override
    public String toString() {
        return new GsonBuilder()
                .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                .create()
                .toJson(this);
    }
}
