package de.app.cms.model.game;

import com.google.gson.GsonBuilder;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "game")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    public Date date;
    public String team1;
    public String team1LocalImageName;
    public String team2LocalImageName;
    public String team2;
    public String team1Short;
    public String team2Short;
    public String result;
    public GameMode gameMode;
    public Finals finals;
    @Column(columnDefinition = "boolean default true", nullable = false)
    public boolean isNextGame;
    public String homeTeamImgUrl;
    public String awayTeamImgUrl;

    @Override
    public String toString() {
        return new GsonBuilder()
                .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                .create()
                .toJson(this);
    }


}
