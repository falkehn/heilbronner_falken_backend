package de.app.cms.model.game;

public enum Finals {
    Runde_1,
    Achtelfinale,
    Viertelfinale,
    Halbfinale,
    Finale,
}
