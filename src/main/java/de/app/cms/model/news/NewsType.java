package de.app.cms.model.news;

public enum NewsType {

    FalkenPage,
    Web,
    YouTube,
    Ticker
}
