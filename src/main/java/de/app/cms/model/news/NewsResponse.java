package de.app.cms.model.news;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import de.app.cms.model.game.Game;

import java.util.Date;
import java.util.List;

public class NewsResponse {

    public static class NewsResponseV2 {

        public List<News> newsItems;

        public NewsResponseV2(List<News> news) {
            this.newsItems = news;
        }

        @Override
        public String toString() {
            return new GsonBuilder()
                    .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                    .create()
                    .toJson(this);
        }
    }

    public static class NewsResponseV3 {

        @SerializedName("next_game")
        public NextGame nextGame;

        @SerializedName("news")
        public List<News> news;

        public NewsResponseV3(NextGame nextGame, List<News> news) {
            this.nextGame = nextGame;
            this.news = news;
        }

        @Override
        public String toString() {
            return new GsonBuilder()
                    .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                    .create()
                    .toJson(this);
        }

        public static class NextGame {

            public String hometeam;
            public String awayteam;
            public Date date;
            public String hometeamImage;
            public String awayteamImage;

            public NextGame(String hometeam, String awayteam, Date date, String hometeamImage, String awayteamImage) {
                this.hometeam = hometeam;
                this.awayteam = awayteam;
                this.date = date;
                this.hometeamImage = hometeamImage;
                this.awayteamImage = awayteamImage;
            }

            @Override
            public String toString() {
                return new GsonBuilder()
                        .setDateFormat("MMM dd, yyyy hh:mm:ss a")
                        .create()
                        .toJson(this);
            }
        }
    }
}
