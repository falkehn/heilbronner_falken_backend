package de.app.cms.push.google;


import com.google.android.gcm.server.*;
import de.app.cms.model.news.NewsType;
import de.app.cms.model.push.PushDevice;
import de.app.cms.push.PushService;
import de.app.cms.push.firebase.FCMRecipientBundle;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class GCMService extends PushService {

    private static final String SENDER_ID = "AIzaSyBvHP9uKAABlGjGCqWzTSdSADzZzRmANTs";
    private static final int NUMBER_OF_PUSH_RETIRES = 1;

    @Override
    public void push(List<PushDevice> pushDevices, String title, String messageText, String id, NewsType dataType, String link) {
        List<PushDevice> invalidPushDevices = new ArrayList<>();
        Sender sender = new Sender("AIzaSyBvHP9uKAABlGjGCqWzTSdSADzZzRmANTs");
        Message pushMessage = new Message.Builder()
                .addData("message", messageText)
                .addData("title", title)
                .addData("id", id)
                .addData("dataType", dataType.toString())
                .addData("link", link)
                .build();

        List<FCMRecipientBundle> FCMRecipientBundles = createGCMRecipientBundlesFromPushDeviceList(pushDevices);

        for (FCMRecipientBundle FCMRecipientBundle : FCMRecipientBundles) {
            try {
                MulticastResult multicastPushResult = sender.send(pushMessage, FCMRecipientBundle.getPushDeviceTokens(), NUMBER_OF_PUSH_RETIRES);
                List<Result> pushResults = multicastPushResult.getResults();
                for (Result pushResult : pushResults) {

                    int indexOfPushResult = pushResults.indexOf(pushResult);
                    PushDevice pushDeviceForPushResult = FCMRecipientBundle.getPushDevices().get(indexOfPushResult);
                    if (pushResult.getMessageId() != null) {
                        String canonicalRegId = pushResult.getCanonicalRegistrationId();
                        if (canonicalRegId != null && !canonicalRegId.isEmpty()) {
                            invalidPushDevices.add(pushDeviceForPushResult);
                        }
                    } else {
                        String error = pushResult.getErrorCodeName();
                        if (error.equals(Constants.ERROR_NOT_REGISTERED) || error.equals(Constants.ERROR_INVALID_REGISTRATION)
                                || error.equals(Constants.ERROR_MISMATCH_SENDER_ID) ) {
                            // Application has been removed from device.
                            // Unregister pushDevice in database.
                            invalidPushDevices.add(pushDeviceForPushResult);
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        deletePushDevices(invalidPushDevices);
    }

    private List<FCMRecipientBundle> createGCMRecipientBundlesFromPushDeviceList(List<PushDevice> pushDeviceList) {
        List<FCMRecipientBundle> FCMRecipientBundles = new ArrayList<>();

        for (PushDevice pushDevice : pushDeviceList) {
            int indexOfPushDevice = pushDeviceList.indexOf(pushDevice);
            if (indexOfPushDevice % 1000 == 0) {
                FCMRecipientBundle newFCMRecipientBundle = new FCMRecipientBundle();
                newFCMRecipientBundle.addPushDevice(pushDevice);
                FCMRecipientBundles.add(newFCMRecipientBundle);
            } else {
                int mostRecentGcmRecipientBundleIndex = FCMRecipientBundles.size() - 1;
                FCMRecipientBundle mostRecentFCMRecipientBundle = FCMRecipientBundles.get(mostRecentGcmRecipientBundleIndex);
                mostRecentFCMRecipientBundle.addPushDevice(pushDevice);
            }
        }

        return FCMRecipientBundles;
    }
}