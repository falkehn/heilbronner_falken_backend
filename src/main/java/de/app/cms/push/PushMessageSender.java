package de.app.cms.push;

import de.app.cms.model.news.NewsType;
import de.app.cms.model.push.PushDevice;
import de.app.cms.push.firebase.FCMService;
import de.app.cms.push.google.GCMService;
import de.app.cms.push.ios.IOSPushService;
import de.app.cms.repo.PushDeviceRepository;
import org.aspectj.weaver.ast.And;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PushMessageSender {

    @Autowired
    private PushDeviceRepository mPushDeviceRepository;

    @Autowired
    private GCMService mGcmService;

    @Autowired
    private IOSPushService mIosService;

    @Autowired
    private FCMService mFcmService;

    public void sendPush(String title, String message, String id, NewsType dataType, String link) {
        final List<PushDevice> iOSDevices = mPushDeviceRepository.findByPlatformType(PushDevice.PlatformType.IOS);
        mIosService.push(iOSDevices, title, message, id, dataType, link);

        final List<PushDevice> androidDevices = mPushDeviceRepository.findByPlatformType(PushDevice.PlatformType.ANDROID);
        mGcmService.push(androidDevices, title, message, id, dataType, link);

        final List<PushDevice> firebaseDevices = mPushDeviceRepository.findByPlatformType(PushDevice.PlatformType.FIREBASE);
        mFcmService.push(firebaseDevices, title, message, id, dataType, link);
    }

    public void sendTestPush(String title, String message, String id, NewsType dataType, String link) {
        final List<PushDevice> firebaseDevices = new ArrayList<>();
        final PushDevice testDevice = new PushDevice();
        testDevice.deviceToken = "ezemwqrVmJw:APA91bHNYmqNCnYtNig4_Z5khxFineKwmVnVi7KDUPAWrW0A0kQquN35stxbPFo-9PjabbIE6L5V8mhNCkV1o6Hi2o_cJ-YToUk6AGZ6qtRqG0fOchR4pYFCdjQXrh1MfDGgrH5z9CgZ";
        testDevice.platformType = PushDevice.PlatformType.ANDROID;
        testDevice.id = 123;
        firebaseDevices.add(testDevice);
        mFcmService.push(firebaseDevices, title, message, id, dataType, link);
    }

    public void sendFirebasePush(String title, String message, String id, NewsType dataType, String link) {
        final List<PushDevice> firebaseDevices = mPushDeviceRepository.findByPlatformType(PushDevice.PlatformType.FIREBASE);
        mFcmService.push(firebaseDevices, title, message, id, dataType, link);
    }
}
