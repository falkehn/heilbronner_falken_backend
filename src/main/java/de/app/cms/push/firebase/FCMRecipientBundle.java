package de.app.cms.push.firebase;

import de.app.cms.model.push.PushDevice;

import java.util.ArrayList;
import java.util.List;

/**
 * This Class is Part of the Project cms Copyright: Lidl E-Commerce
 * GmbH & Co. KG Stiftsbergstr. 1 74172 Neckarsulm
 *
 * @author tomaszadamczyk
 */
public class FCMRecipientBundle {

    private List<PushDevice> pushDevices;

    public FCMRecipientBundle() {
        pushDevices = new ArrayList<PushDevice>();
    }

    public List<PushDevice> getPushDevices() {
        return pushDevices;
    }

    public void addPushDevice(PushDevice pushDevice) {
        pushDevices.add(pushDevice);
    }

    public void removePushDevice(PushDevice pushDevice) {
        pushDevices.remove(pushDevice);
    }

    public List<String> getPushDeviceTokens() {
        List<String> pushDeviceTokens = new ArrayList<String>();
        for(PushDevice pushDevice : pushDevices) {
            pushDeviceTokens.add(pushDevice.deviceToken);
        }
        return pushDeviceTokens;
    }
}
