package de.app.cms.push.firebase;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import de.app.cms.model.news.NewsType;
import de.app.cms.model.push.PushDevice;
import de.app.cms.push.PushService;
import de.app.cms.repo.PushDeviceRepository;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Component
public class FCMService extends PushService {

    private static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";

    @Autowired
    private PushDeviceRepository mPushDeviceRepository;

    @Override
    public void push(List<PushDevice> devices, String title, String message, String id, NewsType dataType, String link) {
        final List<FCMRecipientBundle> FCMRecipientBundles = createGCMRecipientBundlesFromPushDeviceList(devices);

        final Data data = new Data();
        data.title = title;
        data.message = message;
        data.id = id;
        data.dataType = dataType.toString();
        data.link = link;

        for (FCMRecipientBundle fcmRecipientBundle : FCMRecipientBundles) {
            final FCMRequest request = new FCMRequest();
            request.registrationIds = fcmRecipientBundle.getPushDeviceTokens();
            request.data = data;

            post(request);
        }
    }

    private void post(FCMRequest request) {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };

            // Install the all-trusting trust manager
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception e) {
                e.printStackTrace();
            }

            final HttpPost httpPost = new HttpPost(FCM_URL);
            final Header[] headers = new Header[2];
            headers[0] = new BasicHeader("Authorization", "key=AIzaSyBgZWcGHvND8SD_ABciE1u7nnKAjbFm63U");
            headers[1] = new BasicHeader("Content-Type", "application/json");
            httpPost.setHeaders(headers);
            Gson gson = new Gson();
            HttpEntity entity = new ByteArrayEntity(gson.toJson(request).getBytes("UTF-8"));
            httpPost.setEntity(entity);
            final HttpClient httpclient = HttpClients.createDefault();
            final HttpResponse response = httpclient.execute(httpPost);

            if (response.getStatusLine().getStatusCode() == 200) {
                final HttpEntity httpEntity = response.getEntity();
                final StringBuilder responseString = new StringBuilder();
                BufferedReader br = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                String currentLine;
                while ((currentLine = br.readLine()) != null) {
                    responseString.append(currentLine);
                }
                br.close();

                responseString.toString();

                final FCMResponse fcmResponse = gson.fromJson(responseString.toString(), FCMResponse.class);
                handleResponse(request.registrationIds, fcmResponse);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleResponse(final List<String> pushDevices, final FCMResponse fcmResponse) {
        for (int i = 0; i < pushDevices.size(); i++) {
            final Result result = fcmResponse.results.get(i);
            if (result.error != null || result.registrationId != null) {
                PushDevice pushDevice = mPushDeviceRepository.findByDeviceToken(pushDevices.get(i));
                mPushDeviceRepository.delete(pushDevice);
            }
        }
    }

    private List<FCMRecipientBundle> createGCMRecipientBundlesFromPushDeviceList(List<PushDevice> pushDeviceList) {
        List<FCMRecipientBundle> FCMRecipientBundles = new ArrayList<>();

        for (PushDevice pushDevice : pushDeviceList) {
            int indexOfPushDevice = pushDeviceList.indexOf(pushDevice);
            if (indexOfPushDevice % 1000 == 0) {
                FCMRecipientBundle newFCMRecipientBundle = new FCMRecipientBundle();
                newFCMRecipientBundle.addPushDevice(pushDevice);
                FCMRecipientBundles.add(newFCMRecipientBundle);
            } else {
                int mostRecentGcmRecipientBundleIndex = FCMRecipientBundles.size() - 1;
                FCMRecipientBundle mostRecentFCMRecipientBundle = FCMRecipientBundles.get(mostRecentGcmRecipientBundleIndex);
                mostRecentFCMRecipientBundle.addPushDevice(pushDevice);
            }
        }

        return FCMRecipientBundles;
    }

    public static class FCMRequest {

        public Data data;
        @SerializedName("registration_ids")
        public List<String> registrationIds;
    }

    public static class Data {

        public String message;
        public String title;
        public String id;
        public String dataType;
        public String link;
    }

    public static class FCMResponse {

        public List<Result> results;
    }

    public static class Result {
        @SerializedName("message_id")
        public String messageId;
        public String error;
        @SerializedName("registration_id")
        public String registrationId;
    }
}
