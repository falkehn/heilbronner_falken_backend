package de.app.cms.push;


import de.app.cms.model.news.NewsType;
import de.app.cms.model.push.PushDevice;
import de.app.cms.repo.PushDeviceRepository;
import javapns.Push;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class PushService {

    private static final Logger sLogger = LogManager.getLogger(PushService.class);

    @Autowired
    protected PushDeviceRepository mPushDeviceRepository;

    /**
     * Returns old devices to delete
     * @return
     */
    public abstract void push(List<PushDevice> devices, String title, String message, String id, NewsType dataType, String link);

    public void deletePushDevices(List<PushDevice> devicesToDelete) {
        if (devicesToDelete.size() > 0) {
            sLogger.info(String.format("Deleting %d invalid google push devices from db", devicesToDelete.size()));
            for (int i=0; i<devicesToDelete.size(); i++) {
                PushDevice device = devicesToDelete.get(i);
                if (device != null) {
                    mPushDeviceRepository.delete(device);
                }
            }
        }
    }
}
