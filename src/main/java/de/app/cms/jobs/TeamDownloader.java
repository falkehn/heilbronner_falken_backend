package de.app.cms.jobs;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import de.app.cms.holema.HolemaPlayer;
import de.app.cms.model.player.Player;
import de.app.cms.model.player.Position;
import de.app.cms.repo.PlayerRepository;
import de.app.cms.utils.NationUtils;
import de.app.cms.utils.NetUtils;
import de.app.cms.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


@Service
@Configurable
@EnableScheduling
public class TeamDownloader {

    private static final String TEAM_ROSTER_URL = "https://del2.holema.eu/api/teams/roster.json/112";
    private static final String TEAM_STATS_URL = "https://del2.holema.eu/api/teams/stats.json/112";

    @Autowired
    private PlayerRepository mPlayerRepository;

    //15 min
    @Scheduled(fixedDelay = 900000)
    public void downloadTeamData() throws IOException, InterruptedException {
        System.out.println("***** Execute TeamDownloader ******");
        List<Player> players = new ArrayList<>();

        final List<HolemaPlayer> roster = getTeam();

        mPlayerRepository.deleteByTeam("Heilbronner Falken");
        for (final HolemaPlayer holemaPlayer : roster) {
            final int nr = holemaPlayer.number;
            final String firstName = holemaPlayer.firstname;
            final String lastName = holemaPlayer.lastname;
            Player player = mPlayerRepository.findByNumberAndLastNameAndFirstNameAndTeam(nr, lastName, firstName, "Heilbronner Falken");
            if (player == null) {
                player = new Player();
            }
            player.team = "Heilbronner Falken";

            player.number = nr;

            player.firstName = firstName;
            player.lastName = lastName;
            player.nation = NationUtils.getNation(holemaPlayer.nationality);
            player.nationImgUrl = NationUtils.getNationImgUrl(player.nation);

            player.position = getPosition(holemaPlayer.position);
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            try {
                player.birthday = sdf.parse(holemaPlayer.birthday);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            player.size = holemaPlayer.height;
            player.weight = holemaPlayer.weight;
            player.hand = holemaPlayer.hand;
            if (!holemaPlayer.image.src.contains("noimage")) {
                player.largeImageUrl = holemaPlayer.image.src;
                player.imageUrl = holemaPlayer.image.src;
            }
            player.link = "https://www.del-2.org/team/heilbronner_falken/kader/";

            player.gamesPlayed = holemaPlayer.games;
            player.goals = holemaPlayer.goals;
            player.assists = holemaPlayer.assists;
            player.plusMinus = holemaPlayer.plusMinus;
            player.penaltyMinutes = holemaPlayer.penalties;
            player.scorerpoints = holemaPlayer.points;

            try {
                Utils.savePlayer(player, String.format("%d.html", player.number));
                System.out.println("save Player: " + player.firstName + " " + player.lastName);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            players.add(player);

            mPlayerRepository.save(player);
        }

        Player topScorer = null;
        for (Player savedPlayer : players) {
            if (topScorer == null && savedPlayer.scorerpoints > 0) {
                topScorer = savedPlayer;
            } else {
                if (topScorer != null && topScorer.scorerpoints < savedPlayer.scorerpoints) {
                    topScorer = savedPlayer;
                }
            }
            savedPlayer.isTopScorer = false;
            mPlayerRepository.save(savedPlayer);
        }
        if (topScorer != null) {
            topScorer.isTopScorer = true;
            mPlayerRepository.save(topScorer);
        }

    }

    private Position getPosition(String position) {
        if (position.equals("G")) {
            return Position.Goalie;
        } else if (position.equals("D")) {
            return Position.Verteidiger;
        } else if (position.equals("F") || position.equals("C") || position.equals("RW") || position.equals("LW")) {
            return Position.Stürmer;
        } else if (position.equals("Trainer")) {
            return Position.Trainer;
        }
        return null;
    }

    private List<HolemaPlayer> getTeam() {
        final String rawResponse = NetUtils.getResponse(TEAM_ROSTER_URL, NetUtils.USERNAME, NetUtils.PASSWORD);
        final Gson gson = new Gson();
        final JsonObject jsonRoot = gson.fromJson(rawResponse, JsonObject.class);
        final String jsonPlayerArray = jsonRoot.getAsJsonObject("teamroster").getAsJsonObject("players").get("player").toString();
        final Type listType = new TypeToken<List<HolemaPlayer>>() {
        }.getType();
        final List<HolemaPlayer> roster = gson.fromJson(jsonPlayerArray, listType);

        try {
            final List<HolemaPlayer> stats = getStats();
            for (HolemaPlayer player : roster) {
                for (HolemaPlayer statistic : stats) {
                    if (player.id.equals(statistic.id)) {
                        player.games = statistic.games;
                        player.goals = statistic.goals;
                        player.assists = statistic.assists;
                        player.plusMinus = statistic.plusMinus;
                        player.points = statistic.points;
                        player.penalties = statistic.penalties;
                    }
                }
            }
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        return roster;
    }

    private List<HolemaPlayer> getStats() {
        final String rawResponse = NetUtils.getResponse(TEAM_STATS_URL, NetUtils.USERNAME, NetUtils.PASSWORD);
        final Gson gson = new Gson();
        final JsonObject jsonRoot = gson.fromJson(rawResponse, JsonObject.class);
        final JsonObject playerStats = jsonRoot.getAsJsonObject("playerstats");
        final JsonObject players = playerStats.getAsJsonObject("players");
        final JsonArray player = players.getAsJsonArray("player");
        final String jsonPlayerArray = player.toString();
        final Type listType = new TypeToken<List<HolemaPlayer>>() {}.getType();
        return gson.fromJson(jsonPlayerArray, listType);
    }
}
