package de.app.cms.jobs;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import de.app.cms.holema.HolemaTeam;
import de.app.cms.model.team.Team;
import de.app.cms.repo.TeamRepository;
import de.app.cms.utils.ImageUtils;
import de.app.cms.utils.NetUtils;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@EnableScheduling
public class StandingsDownloader {

    private static final String STANDINGS_URL = "https://del2.holema.eu/api/teams/standings.json/112";

    @Autowired
    private TeamRepository mTeamRepository;

    //15 min
    @Scheduled(fixedDelay = 900000)
    public void execute() throws JobExecutionException {
        System.out.println("***** Execute StandingsDownloader ******");

        final String rawResponse = NetUtils.getResponse(STANDINGS_URL, NetUtils.USERNAME, NetUtils.PASSWORD);
        final Gson gson = new Gson();
        final JsonObject jsonRoot = gson.fromJson(rawResponse, JsonObject.class);
        final String jsonTeamArray = jsonRoot.getAsJsonObject("teamstats").getAsJsonObject("teams").get("team").toString();
        final Type listType = new TypeToken<List<HolemaTeam>>() {}.getType();
        final List<HolemaTeam> holemaTeams = gson.fromJson(jsonTeamArray, listType);

        ArrayList<Team> teams = new ArrayList<>();

        //Durchlaufe alle Tabellen-Position (0 == Legende, deshalb uebersprungen)
        //Pos, Verein, Spiele, Siege, Niederlagen, OTS, OTN, Tore, Punkte
        for (final HolemaTeam holemaTeam : holemaTeams) {
            Team team = new Team();
            team.gamesCount = holemaTeam.games;
            team.goalsShot = holemaTeam.goalsfor;
            team.goalsGet = holemaTeam.goalsagainst;
            team.name = holemaTeam.name;
            team.localImageName = ImageUtils.getServerImageName(holemaTeam.name);
            team.points = holemaTeam.points;
            teams.add(team);
        }

        for (Team team : teams) {
            int goalDif = team.goalsShot - team.goalsGet;
            team.goals = (team.goalsShot + ":" + team.goalsGet + " (" + goalDif + ")");
        }

        if (teams.size() > 0) {
            mTeamRepository.deleteAll();
            Collections.sort(teams, new StandingsComparator());
            mTeamRepository.save(teams);
        }

        System.out.println("***** Finished StandingsDownloader ******");
    }

    private class StandingsComparator implements Comparator<Team> {

        @Override
        public int compare(Team t1, Team t2) {
            int points1 = t1.points;
            int points2 = t2.points;
            if (points1 > points2) return -1;
            else if (points1 < points2) return 1;
            else return 0;
        }
    }
}
