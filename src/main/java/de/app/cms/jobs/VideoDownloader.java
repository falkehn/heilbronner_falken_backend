package de.app.cms.jobs;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import de.app.cms.model.news.NewsType;
import de.app.cms.model.video.Video;
import de.app.cms.push.PushMessageSender;
import de.app.cms.repo.VideoRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@Configurable
@EnableScheduling
public class VideoDownloader {

    private static final String CHANNEL_URL = "https://www.googleapis.com/youtube/v3/channels";
    private static final String VIDEO_LIST = "https://www.googleapis.com/youtube/v3/playlistItems";
    private static final String DEVELOPER_KEY = "AIzaSyDXyq1H9WkxKi5Fm34Btspe-cOiwFNbK8U";

    @Autowired
    private VideoRepository mVideoRepository;

    @Autowired
    private PushMessageSender mPushMessageSender;

    //15 min
    @Scheduled(fixedDelay = 900000)
    public void execute() throws JobExecutionException {
        System.out.println("***** Execute VideoDownlaoder ******");

        getSpradeTvHighlights();
        getHNFalkenVideos();

        System.out.println("***** Finished VideoDownlaoder ******");
    }

    private void getSpradeTvHighlights() {
        // get channel informations
        try {
            final String channelQuery = "part=contentDetails&forUsername=spradetv&key=" + DEVELOPER_KEY;
            final JsonArray items = getJsonVideoItems(channelQuery);

            for (int i = 0; i < items.size(); i++) {
                final JsonObject video = items.get(i).getAsJsonObject().get("snippet").getAsJsonObject();
                final String title = video.get("title").getAsString();
                if (title.contains("Heilbronn")) {
                    parseVideo(video);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getHNFalkenVideos() {
        // get channel informations
        try {
            final String channelQuery = "part=contentDetails&forUsername=HNFalkenTV&key=" + DEVELOPER_KEY;
            final JsonArray items = getJsonVideoItems(channelQuery);

            for (int i = 0; i < items.size(); i++) {
                final JsonObject video = items.get(i).getAsJsonObject().get("snippet").getAsJsonObject();
                parseVideo(video);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseVideo(final JsonObject video) throws Exception {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        final String title = video.get("title").getAsString();
        final String dateStr = video.get("publishedAt").getAsString();
        Date date = sdf.parse(dateStr);
        final String videoId = video.get("resourceId").getAsJsonObject().get("videoId").getAsString();
        final String link = "https://m.youtube.com/details?v=" + videoId;
        final String thumbnail = video.get("thumbnails").getAsJsonObject().get("high").getAsJsonObject().get("url").getAsString();
        final String description = video.get("description").getAsString();

        Video videoToSave = mVideoRepository.findByYoutubeVideoId(videoId);
        boolean push = false;
        if (videoToSave == null) {
            push = true;
            videoToSave = new Video();
            videoToSave.youtubeVideoId = videoId;
        }
        videoToSave.title = title;
        videoToSave.iosUrl = link;
        videoToSave.androidUrl = link;
        videoToSave.date = date;
        videoToSave.imgUrl = thumbnail;
        videoToSave.duration = 0;
        videoToSave.secondaryTitle = description;

        videoToSave = mVideoRepository.save(videoToSave);

        if (push) {
            mPushMessageSender.sendPush(title, description, String.valueOf(videoToSave.id), NewsType.YouTube, link);
        }
    }

    private JsonArray getJsonVideoItems(final String channelQuery) throws Exception {
        HttpURLConnection connection = (HttpURLConnection) new URL(CHANNEL_URL + "?" + channelQuery).openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));

        StringBuilder builder = new StringBuilder();
        for (String line; (line = reader.readLine()) != null; ) {
            builder.append(line);
        }

        reader.close();

        Gson gson = new Gson();
        JsonObject jsonObject = gson.fromJson(builder.toString(), JsonObject.class);
        JsonArray items = jsonObject.getAsJsonObject().get("items").getAsJsonArray();
        String uploadId = items.get(0).getAsJsonObject().get("contentDetails")
                .getAsJsonObject().get("relatedPlaylists")
                .getAsJsonObject().get("uploads").getAsString();

        // get videos
        String videoQuery = "part=snippet&maxResults=50&playlistId=" + uploadId + "&key=" + DEVELOPER_KEY;
        connection = (HttpURLConnection) new URL(VIDEO_LIST + "?" + videoQuery).openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        builder = new StringBuilder();
        reader = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        for (String line; (line = reader.readLine()) != null; ) {
            builder.append(line);
        }

        reader.close();

        jsonObject = gson.fromJson(builder.toString(), JsonObject.class);
        return jsonObject.get("items").getAsJsonArray();
    }
}