package de.app.cms.jobs;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import de.app.cms.holema.HolemaGame;
import de.app.cms.model.game.Game;
import de.app.cms.model.game.GameMode;
import de.app.cms.repo.GameRepository;
import de.app.cms.utils.ImageUtils;
import de.app.cms.utils.NetUtils;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Service
@Configurable
@EnableScheduling
public class SeasonScheduleDownloader {

    private static final String URL = "http://del2.holema.eu/api/teams/games.json/";

    @Autowired
    private GameRepository mGameRepository;

    @Scheduled(fixedDelay = 900000)
    public void execute() throws JobExecutionException {
        System.out.println("***** Execute SeasonScheduleDownloader ******");
        mGameRepository.deleteAll();

//        final int testspiele = 105;
//        final List<HolemaGame> testround = getHolemaGames(testspiele);
        final int hauptrunde = 112;
        final List<HolemaGame> mainround = getHolemaGames(hauptrunde);
//        if (testround != null && testround.size() > 0 || mainround != null && mainround.size() > 0) {
//            mGameRepository.deleteAll();
//        }

//        for (final HolemaGame holemaGame : testround) {
//            final Game game = parse(holemaGame, GameMode.Preperation);
//            mGameRepository.save(game);
//        }

        for (final HolemaGame holemaGame : mainround) {
            final Game game = parse(holemaGame, GameMode.MainRound);
            mGameRepository.save(game);
        }

//        final int playdowns = 43;
//        final List<HolemaGame> playdownsround = getHolemaGames(playdowns);
//        for (final HolemaGame holemaGame : playdownsround) {
//            final Game game = parse(holemaGame, GameMode.Playdowns);
//            mGameRepository.save(game);
//        }

        // find out next game
        List<Game> games = mGameRepository.findAll();
        Game nextgame = null;
        long now = Calendar.getInstance().getTime().getTime();
        if(games != null) {
            for (Game game : games) {
                long eventMillis = game.date.getTime();
                long diffEvent = eventMillis - now;
                if(diffEvent >=0 && (nextgame == null || eventMillis < nextgame.date.getTime())){
                    nextgame = game;
                }
            }
        }
        if (nextgame != null) {
            nextgame.isNextGame = true;
        }
        mGameRepository.save(nextgame);

        System.out.println("***** Finished SeasonScheduleDownloader ******");
    }

    public Game parse(final HolemaGame holemaGame, final GameMode mode) {
        final Game game = new Game();

        final String dateWithTime = holemaGame.gamedate + " " + holemaGame.gametime;
        final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        try {
            game.date = sdf.parse(dateWithTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        game.team1 = holemaGame.hometeam.name;
        game.team2 = holemaGame.awayteam.name;
        if (holemaGame.homescore == null) {
            game.result = "-:-";
        } else {
            game.result = holemaGame.homescore + ":" + holemaGame.awayscore;
        }
        game.team1LocalImageName = ImageUtils.getGameImageName(holemaGame.hometeam.name);
        game.team2LocalImageName = ImageUtils.getGameImageName(holemaGame.awayteam.name);
        game.homeTeamImgUrl = ImageUtils.getServerImageName(holemaGame.hometeam.name);
        game.awayTeamImgUrl = ImageUtils.getServerImageName(holemaGame.awayteam.name);
        game.team1Short = holemaGame.hometeam.shortname;
        game.team2Short = holemaGame.awayteam.shortname;
        game.gameMode = mode;
        return game;
    }

    public List<HolemaGame> getHolemaGames(final int roundId) {
        final String rawResponse = NetUtils.getResponse(URL + roundId, NetUtils.USERNAME, NetUtils.PASSWORD);
        final Gson gson = new Gson();
        final JsonObject jsonRoot = gson.fromJson(rawResponse, JsonObject.class);
        final String jsonSeasonScheduleArray = jsonRoot.getAsJsonObject("schedule").getAsJsonObject("games").get("game").toString();
        final Type listType = new TypeToken<List<HolemaGame>>() {}.getType();
        return gson.fromJson(jsonSeasonScheduleArray, listType);
    }
}
