package de.app.cms.utils;

import de.app.cms.model.news.News;
import de.app.cms.model.news.NewsType;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Calendar;
import java.util.Date;

/**
 * This Class is Part of the Project cms Copyright: Lidl E-Commerce
 * GmbH & Co. KG Stiftsbergstr. 1 74172 Neckarsulm
 *
 * @author tomaszadamczyk
 */
public class UrlPageUtils {

    public static News newsForPageUrl(String url) {
        Document document;
            document = Utils.get(url);
            String title = getMetaTag(document, "og:title");
            if(title.length() == 0){
                Elements titleElement = document.select("title");
                title = titleElement.text();
            }
            String image = getMetaTag(document, "og:image");
            Date currentDate = Calendar.getInstance().getTime();
            String id = String.valueOf(url.hashCode());

            String description = getMetaTag(document, "description");
            int maxLength = 150;
            if (Math.min(description.length(), maxLength) == maxLength) {
                description = description.substring(0, Math.min(description.length(), maxLength)) + "...";
            }

            News news = new News();
            news.setTitle(title.trim());
            news.setSecondaryTitle(description.trim());
            news.setImgUrl(image);
            news.setDate(currentDate);
            news.setNewsType(NewsType.Web);
            news.setLink(Utils.shortenUrl(url));

            return news;
    }

    private static String getMetaTag(Document document, String attr){
        if(document == null){
            return "";
        }
        Elements elements = document.select("meta[name=" + attr + "]");
        for (Element element : elements) {
            final String s = element.attr("content");
            if (s != null) return s;
        }
        elements = document.select("meta[property=" + attr + "]");
        for (Element element : elements) {
            final String s = element.attr("content");
            if (s != null) return s;
        }
        return "";
    }
}
