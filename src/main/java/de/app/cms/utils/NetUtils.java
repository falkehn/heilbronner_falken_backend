package de.app.cms.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.CoreProtocolPNames;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetUtils {

    public static final String USERNAME = "heilbronn";
    public static final String PASSWORD = "0FvVZdGGJ85WOFw7MdYj";

    private static final int OK = 200;

    public static String getResponse(final String url) {
        try {
            final HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader(CoreProtocolPNames.HTTP_CONTENT_CHARSET, String.valueOf(Consts.UTF_8));
            final HttpClient httpclient = HttpClients.createDefault();
            final HttpResponse response = httpclient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == OK) {
                final HttpEntity httpEntity = response.getEntity();
                final StringBuilder responseString = new StringBuilder();
                BufferedReader br = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                String currentLine;
                while ((currentLine = br.readLine()) != null) {
                    responseString.append(currentLine);
                }
                br.close();

                return responseString.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getResponse(final String stringUrl, final String userName, final String password) {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };

            // Install the all-trusting trust manager
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception e) {
            }

            final URL url = new URL(stringUrl);

            final HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            uc.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36");

            final String userpass = userName + ":" + password;
            final String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
            uc.setRequestProperty ("Authorization", basicAuth);

            uc.connect();

            if (uc.getResponseCode() == OK) {
                final BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
                String inputLine;
                final StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                return response.toString();
            } else {
                Utils.sendMail("URL: " + url + "\nStatus: " + uc.getResponseCode(), "Download Fehlgeschlagen.");
            }
        } catch (IOException e) {
            e.printStackTrace();

            final StringBuilder builder = new StringBuilder();
            builder.append(e);

            StackTraceElement[] trace = e.getStackTrace();
            for (StackTraceElement traceElement : trace) {
                builder.append("\tat " + traceElement);
            }

            // Print suppressed exceptions, if any
            for (Throwable se : e.getSuppressed()) {
                builder.append(se.getMessage());
            }

            // Print cause, if any
            Throwable ourCause = e.getCause();
            if (ourCause != null) {
                builder.append(ourCause.getMessage());
            }

//            Utils.sendMail("URL: " + stringUrl + "\nException: " + builder.toString(), "Download Fehlgeschlagen.");
        }
        return null;
    }
}