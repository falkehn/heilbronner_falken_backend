package de.app.cms.utils;

import de.app.cms.model.player.Nation;

public class NationUtils {

    public static String getName(Nation nation) {
        switch (nation) {
            case Canada:
                return "Kanada";

            case Czech:
                return "Tschechien";

            case German:
                return "Deutsch";

            case Poland:
                return "Polen";

            case Russian:
                return "Russland";

            case Slowakai:
                return "Slowakei";

            case Sweden:
                return "Schweden";

            case Ukraine:
                return "Ukraine";

            case USA:
                return "USA";

            case Wales:
                return "Wales";

            case Finnland:
                return "Finnland";

            default:
                return "-";

        }
    }

    public static Nation getNation(String nation) {
        if (nation.startsWith("(Deutschland") || nation.startsWith("GER")) {
            return Nation.German;
        } else if (nation.startsWith("(Kanada") || nation.startsWith("CAN")) {
            return Nation.Canada;
        } else if (nation.startsWith("(Tschechien") || nation.startsWith("CZE")) {
            return Nation.Czech;
        } else if (nation.startsWith("(Lettland") || nation.startsWith("LAT")) {
            return Nation.Latvia;
        } else if (nation.startsWith("(Polen")) {
            return Nation.Poland;
        } else if (nation.startsWith("(Russland")) {
            return Nation.Russian;
        } else if (nation.startsWith("(Slowakei") || nation.startsWith("SVN")) {
            return Nation.Slowakai;
        } else if (nation.startsWith("(Schweden")) {
            return Nation.Sweden;
        } else if (nation.startsWith("(Ukraine")) {
            return Nation.Ukraine;
        } else if (nation.startsWith("(USA") || nation.startsWith("(Amerika")) {
            return Nation.USA;
        } else if (nation.startsWith("(Weißrussland")) {
            return Nation.WhiteRussia;
        } else if (nation.startsWith("(Großbritannien/Wales)") || nation.startsWith("GBR")) {
            return Nation.Wales;
        } else if (nation.startsWith("(Frankreich)") || nation.startsWith("FRA")) {
            return Nation.France;
        } else if (nation.startsWith("(Finnland)")) {
            return Nation.Finnland;
        }
        return Nation.German;
    }

    public static String getNationImgUrl(Nation nation) {
        String baseUrl = "http://falkenapp.de/storage/flags/";
        switch (nation) {
            case Canada:
                return baseUrl + "canada.png";
            case Czech:
                return baseUrl + "czech.png";
            case German:
                return baseUrl + "germany.png";
            case Russian:
                return baseUrl + "russia.png";
            case Slowakai:
                return baseUrl + "slovakei.png";
            case Sweden:
                return baseUrl + "sweden.png";
            case Ukraine:
                return baseUrl + "ukraine.png";
            case USA:
                return baseUrl + "usa.png";
            case Wales:
                return baseUrl + "wales.png";
            case France:
                return baseUrl + "france.png";
            case Finnland:
                return baseUrl + "finnland.png";
            default:
                return "-";
        }
    }
}
