package de.app.cms.utils;

public class ImageUtils {

    public static String getServerImageName(String team) {
        return  "http://falkenapp.de/storage/logos/" + getGameImageName(team);
    }

    public static String getGameImageName(String team){
        if(team.equals("Heilbronner Falken")){
            return "game_heilbronner_falken.png";
        } else if(team.equals("Bietigheim Steelers")){
            return "game_bietigheim_steelers.png";
        } else if(team.equals("Hannover Indians")){
            return "game_hannover_indians.png";
        } else if(team.equals("EVL Landshut Eishockey")){
            return "game_landshut_cannibals.png";
        } else if(team.equals("SC Riessersee")){
            return "game_sc_riessersee.png";
        } else if(team.equals("Starbulls Rosenheim")){
            return "game_starbulls_rosenheim.png";
        } else if(team.equals("ESV Kaufbeuren")){
            return "game_esv_kaufbeuren.png";
        } else if(team.equals("Fischtown Pinguins Bremerhaven") || team.equals("Pinguins Bremerhaven")){
            return "game_fischtown_pinguins.png";
        } else if(team.equals("Fischtown Pinguins")){
            return "game_fischtown_pinguins.png";
        }else if(team.equals("Lausitzer Füchse")){
            return "game_lausitzer_fuechse.png";
        } else if(team.equals("Ravensburg Towerstars")){
            return "game_towerstars_ravensburg.png";
        } else if(team.equals("Eispiraten Crimmitschau")){
            return "game_eispiraten_crimmitschau.png";
        } else if(team.equals("Dresdner Eislöwen")){
            return "game_dresdner_eisloewen.png";
        } else if(team.equals("Schwenninger Wild Wings")){
            return "game_schwenninger_wild_wings.png";
        } else if(team.equals("Etoile Noire de Strasbourg") || team.equals("L´Etoile Noire Strasbourg")){
            return "game_strasbourg.png";
        } else if(team.equals("EC Bad Nauheim")){
            return "game_ec_bad_nauheim.png";
        } else if(team.equals("Adler Mannheim")){
            return "game_adler_mannheim.png";
        } else if(team.equals("Löwen Frankfurt")){
            return "game_loewen_frankfurt.png";
        } else if(team.equals("EHC Bayreuth Tigers") || team.equals("EHC Bayreuth") || team.equals("EHC Bayreuth - Tigers") || team.equals("Bayreuth Tigers")){
            return "game_ehc_bayreuth.png";
        } else if(team.equals("EHC Wölfe Freiburg") || team.equals("Wölfe Freiburg") || team.equals("EHC Freiburg")){
            return "game_woelfe_freiburg.png";
        } else if(team.equals("Kassel Huskies")){
            return "game_kassel_huskies.png";
        } else if(team.equals("EC Kassel Huskies")){
            return "game_kassel_huskies.png";
        } else if (team.equals("Bulldogs Dornbirn")) {
            return "game_bulldogs_dornbirn.png";
        } else if (team.equals("ERC Sonthofen")) {
            return "game_erc_sonthofen.png";
        } else if (team.equals("Füchse Duisburg")) {
            return "game_fuechse_duisburg.png";
        } else if (team.equals("Tölzer Löwen")) {
            return "game_toelzer_loewen.png";
        } else if (team.equals("SC Deggendorf") || team.equals("Deggendorfer SC")) {
            return "game_deggendorfer_sc.png";
        }
        return null;
    }
}
