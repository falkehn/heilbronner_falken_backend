package de.app.cms.utils;

import de.app.cms.controller.model.AppConfiguration;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * This Class is Part of the Project cms Copyright: Lidl E-Commerce
 * GmbH & Co. KG Stiftsbergstr. 1 74172 Neckarsulm
 *
 * @author tomaszadamczyk
 */
public class ProxyCrawler {

    public static void updateProxy() {
        try {
            Document doc =Jsoup.connect("http://www.proxynova.com/proxy-server-list/country-de/")
                    .timeout(100000)
                    .userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36")
                    .validateTLSCertificates(false)
                    .get();
            Element proxyTable = doc.select("#tbl_proxy_list").first();
            Elements proxyRows = proxyTable.select("tr");
            for (int i=1; i<proxyRows.size(); i++) {
                Element proxyRow = proxyRows.get(i);
                //finde einen mit hohen uptime
                Elements proxyColumns = proxyRow.select("td");
                if (proxyColumns.size() > 6) {
                    final String anonymity = proxyColumns.get(6).text().trim();
                    final boolean isAnonym = anonymity.equals("Anonymous");
                    final String percentageLine = proxyColumns.get(4).text();
                    String percantageText = "";
                    for (int j = 0; j < percentageLine.length(); j++) {
                        if (percentageLine.charAt(j) != '%') {
                            percantageText = percantageText + percentageLine.charAt(j);
                        } else {
                            break;
                        }
                    }
                    int uptimeInPercent = Integer.valueOf(percantageText);
                    if (uptimeInPercent > 10 && isAnonym) {
                        String url = proxyColumns.get(0).text();
                        String port = proxyColumns.get(1).text();
                        AppConfiguration configuration = AppConfiguration.read();
                        if (!url.equals(configuration.getProxyUrl())) {
                            configuration.setProxyUrl(url);
                            configuration.setProxyPort(Integer.valueOf(port));
                            AppConfiguration.save(configuration);

                            System.out.println("Save new Proxy: " + url + ":" + port);
                            return;
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
