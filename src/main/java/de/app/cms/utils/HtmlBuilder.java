package de.app.cms.utils;

import de.app.cms.model.image.BannerImage;
import de.app.cms.model.news.News;
import de.app.cms.model.player.Player;
import de.app.cms.model.player.Position;

import java.text.SimpleDateFormat;

public class HtmlBuilder {

    public static final String COLOR_BLACK 		= "#000000";
    public static final String COLOR_GREY_LITE 	= "#727372";
    public static final String COLOR_RED = "#DB0132";
    public static final String COLOR_BLUE = "#0057A3";

    public static final String FONT_BOLD 	= "bold";
    public static final String FONT_ITALIC 	= "italic";
    public static final String FONT_NORMAL	= "normal";
    public static final String FONT_LITE	= "lite";

    public static final String ALIGN_JUSTIFY = "justify";
    public static final String ALIGN_LEFT = "left";
    public static final String ALIGN_RIGHT = "right";
    public static final String ALIGN_CENTER = "center";

    private StringBuilder mHtml = new StringBuilder();

    public void reset() {
        mHtml.setLength(0);
    }

    public void addText(String text, String textColor, String textStyle, int fontSize, String alignment){
        if(text != null && !text.isEmpty()){
            mHtml.append("<div style=\"font-size:"+ fontSize + "px; font-family:arial; font-weight:" + textStyle
                    + "; color:" + textColor +  "; text-align:" + alignment + "; padding-left: 10px; padding-right: 10px; padding-top:5px\">");
            mHtml.append(text);
            mHtml.append("</div>");
        }
    }

    public void addLine(String color){
        mHtml.append("<hr size=\"1\" color=\"" + color + "\" noshade>");
    }

    public void addElementWithText(String tag, String text, boolean linebreak) {
        if(text != null){
            mHtml.append("<" + tag + ">");
            mHtml.append(text);
            mHtml.append("</" + tag + ">");
            if(linebreak){
                addSpace();
            }
        } else {
            System.out.println("Parameter Text is null!");
        }
    }

    public void addParagraph(String text) {
        if(!text.isEmpty()){
            addElementWithText("p", text, false);
        }
    }

    public void addHeader2(String text) {
        if(!text.isEmpty()){
            addElementWithText("h2", text, false);
        }
    }

    public void addHeader3(String text) {
        if(!text.isEmpty()){
            addElementWithText("h3", text, false);
        }
    }

    public void addLine(String text, boolean linebreak) {
        if(!text.isEmpty()){
            addElementWithText("span", text, false);
            if(linebreak){
                mHtml.append("<br/>");
            }
        }
    }

    public void addBoldLine(String text) {
        if(!text.isEmpty()){
            addElementWithText("span", "<b>" + text + "</b><br/>", false);
        }
    }

    public void addKeyValue(String key, String value) {
        if(!key.isEmpty() && !value.isEmpty()){
            addElementWithText("span","<b>" + key + "</b> &nbsp; " + value + "<br/>", false);
        }
    }

    public void addSpace() {
        mHtml.append("<div><br/></div>");
    }

    public void addHtml(String html) {
        if(!html.isEmpty()){
            mHtml.append(html);
        }
    }

    public void addImage(String src) {
        if(src != null && !src.isEmpty()){
            mHtml.append("<div id=\"container\" class=\"content\">");
            mHtml.append("<img src=\"" + src + "\" width=\"100%\"'/>");
            mHtml.append("</div>");
        }
    }

    public void addLink(String url, String text) {
        mHtml.append("<a href=\"" + url + "\">" + text + "</a>");
    }

    public String renderBody() {
        return mHtml.toString();
    }

    public static String getPlayerHtml(Player player){
        HtmlBuilder builder = new HtmlBuilder();
        builder.reset();

        Utils utils = new Utils();
        String htmlTemplate = utils.getHtmlSkeleton("player_skeleton.html");
        String playerFirstLine = String.format("%s %s #%d", player.firstName, player.lastName, player.number);
        String playerPosition = player.position.toString();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        String playerBirthday = sdf.format(player.birthday);
        String playerNation = NationUtils.getName(player.nation);


        String playerSizeWeigth = String.format("%d cm/ %d kg", player.size, player.weight);
        String playerShootHand = player.hand;
        String playerScorerPoints = String.valueOf(player.scorerpoints);
        String playerGoals = String.valueOf(player.goals);
        String playerAssists = String.valueOf(player.assists);
        String playerFalkeSince = player.falkeSince;
        String img = player.largeImageUrl.length() == 0 ? "http://falkenapp.h2149152.stratoserver.net/Falken/Team/placeholder_player.png" : player.largeImageUrl;

        String retVal;
        if(player.position != Position.Trainer){
            retVal =  String.format(htmlTemplate, playerFirstLine, img, playerPosition, playerBirthday, playerNation, playerSizeWeigth, playerShootHand, playerScorerPoints,
                    playerGoals, playerAssists, playerFalkeSince);
        } else {
            retVal =  String.format(htmlTemplate, player.firstName + " " + player.lastName, img, playerPosition, playerBirthday, playerNation, "", "", "", "", "", playerFalkeSince);
        }
        return retVal;
    }

    public static String getMailHtml(BannerImage bannerImage){
        HtmlBuilder builder = new HtmlBuilder();
        builder.reset();

        builder.addHeader3("Neues Falken Bild wurde hochgeladen");
        builder.addSpace();
        builder.addLine(COLOR_BLACK);
        builder.addImage(bannerImage.url);
        builder.addSpace();

        builder.addLink("http://falkenapp.de/cms/rest/newBanner?id=" + bannerImage.id, "Diesen Banner freigeben?");

        String htmlTemplate = getHtmlSkeleton();
        String retVal = htmlTemplate.replace("INSERT_HTML_HERE", builder.renderBody());

        return retVal;
    }

    public static String getMailHtml(News news, String originalLink){
        HtmlBuilder builder = new HtmlBuilder();
        builder.reset();

        builder.addHeader3("Title: " + news.getTitle());
        builder.addSpace();
        builder.addLine(COLOR_BLACK);
        builder.addText("Secondary Title: " + news.getSecondaryTitle(), COLOR_BLACK, FONT_NORMAL, 16, ALIGN_LEFT);
        builder.addSpace();
        builder.addLink(news.getLink(), "HTML-Link");
        builder.addSpace();
        builder.addLink(originalLink, "Original-Link");
        builder.addSpace();

        builder.addLink("http://falkenapp.de/cms/rest/publish?id=" + news.getId(), "Diese News veröffentlichen?");

        String htmlTemplate = getHtmlSkeleton();
        String retVal = htmlTemplate.replace("INSERT_HTML_HERE", builder.renderBody());

        return retVal;
    }

    public static String getAndroidMailHtml(News news, String originalLink){
        HtmlBuilder builder = new HtmlBuilder();
        builder.reset();

        builder.addHeader3("Title: " + news.getTitle());
        builder.addSpace();
        builder.addLine(COLOR_BLACK);
        builder.addText("Secondary Title: " + news.getSecondaryTitle(), COLOR_BLACK, FONT_NORMAL, 16, ALIGN_LEFT);
        builder.addSpace();
        builder.addLink(news.getLink(), "HTML-Link");
        builder.addSpace();
        builder.addLink(originalLink, "Original-Link");
        builder.addSpace();

        builder.addLink("http://falkenapp.de/cms/rest/publishForAndroid?id=" + news.getId(), "Diese News veröffentlichen?");

        String htmlTemplate = getHtmlSkeleton();
        String retVal = htmlTemplate.replace("INSERT_HTML_HERE", builder.renderBody());

        return retVal;
    }

    public static String getHtmlSkeleton() {
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head lang=\"en\">\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n" +
                "    <style>\n" +
                "        img {\n" +
                "            width: 100%;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "INSERT_HTML_HERE\n" +
                "</body>\n" +
                "</html>";
    }
}
