package de.app.cms.utils;

import de.app.cms.controller.AppConfig;
import de.app.cms.controller.model.AppConfiguration;
import de.app.cms.model.player.Player;
import de.app.cms.model.player.Position;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class DEL2Stats {

    private static int GAMES = 2;
    private static int GOALS = 3;
    private static int ASSISTS = 4;
    private static int SCORER_POINTS = 5;
    private static int PLUS_MINUS = 6;
    private static int PENALTY_MINUTES = 8;

    public static List<String> teamUrls() throws IOException {
        List<String> retVal = new ArrayList<>();
        final String url = "https://www.del-2.org/";
        Document doc = getDocument(url);
        Elements clubs = doc.select(".clubs").select("a");
        for (Element element : clubs) {
            String teamName = element.attr("class");
            if (!retVal.contains(teamName) && teamName.length() > 0) {
                retVal.add(teamName);
            }
        }
        return retVal;
    }

    public static List<String> teamNames() throws IOException {
        List<String> retVal = new ArrayList<>();
        final String url = "https://www.del-2.org/";
        Document doc = getDocument(url);
        Elements clubs = doc.select(".clubs").select("a");
        for (Element element : clubs) {
            String teamName = element.attr("title");
            if (!retVal.contains(teamName) && teamName.length() > 0) {
                retVal.add(teamName);
            }
        }
        return retVal;
    }

    public static List<Player> teamStatsDownloader(String teamUrl, String teamName) throws IOException, InterruptedException {
        System.out.println("Download for: " + teamName);
        final String url = String.format("https://www.del-2.org/team/%s/kader/33", teamUrl);
        Document doc = getDocument(url);
        if (doc == null) {
            return new ArrayList<>();
        }

        List<Player> players = new ArrayList<>();
        Elements tableElements = doc.select(".player_table");
        Elements tableRow = tableElements.select("tbody");
        for (int i = 0; i < tableRow.size(); i++) {
            Element row = tableRow.get(i);
            Elements rowElements = row.select("tbody > tr");
            for (int j = 0; j < rowElements.size(); j++) {
                Element rowElement = rowElements.get(j);
                Elements cells = rowElement.select("td");
                Player player;
                //25.9.15 aenderung der darstellung, goalies haben nun andere spalten wie die spieler
                //Goalie
                final String nameOrPosition = cells.get(1).text();
                boolean isGoalie = nameOrPosition.split(" ").length > 1;
                if (isGoalie) {
                    player = getGoalie(cells);
                } else {
                    player = getFieldPlayer(cells);
                }
                player.team = teamName;
                System.out.println("--> " + player.firstName + " " + player.lastName);
                players.add(player);

                Thread.sleep(750);
            }
        }

        return players;
    }

    private static Player getGoalie(Elements elements) {
        Player player = new Player();
        player.position = Position.Goalie;

        for (int i = 0; i < elements.size(); i++) {
            Element cell = elements.get(i);
            if (i == 0) {
                String numberStr = cell.text();
                try {
                    player.number = Integer.valueOf(numberStr);
                } catch (NumberFormatException e) {
                }
            } else if (i == 1) {
                Element nameAndUrl = cell.select("a").first();
                if (nameAndUrl != null) {
                    String[] name = nameAndUrl.text().split(" ");
                    player.lastName = name[1];
                    player.firstName = name[0];
                    player.link = "https://www.del-2.org/" + nameAndUrl.attr("href");
                }
            } else if (i == 2) {
                if (cell.text().equals("L")) {
                    player.hand = "Links";
                } else {
                    player.hand = "Rechts";
                }
            } else if (i == 3) {
                String nation = cell.attr("title");
                player.nation = NationUtils.getNation(nation);
                player.nationImgUrl = NationUtils.getNationImgUrl(player.nation);
            } else if (i == 4) {
                String size = cell.text();
                try {
                    player.size = Integer.parseInt(size.replaceAll("[\\D]", ""));
                } catch (NumberFormatException e) {
                }
            } else if (i == 5) {
                String weight = cell.text();
                try {
                    player.weight = Integer.parseInt(weight.replaceAll("[\\D]", ""));
                } catch (NumberFormatException e) {
                }
            } else if (i == 6) {
                String birthday = cell.text();
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                try {
                    player.birthday = sdf.parse(birthday);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        player = getPlayerStats(player);
        return player;
    }

    private static Player getFieldPlayer(Elements elements) {
        Player player = new Player();

        for (int i = 0; i < elements.size(); i++) {
            final Element cell = elements.get(i);
            if (i == 0) {
                // Number
                String numberStr = cell.text();
                try {
                    player.number = Integer.valueOf(numberStr);
                } catch (NumberFormatException e) {
                }
            } else if (i == 1) {
                // Position
                String position = cell.text();
                if (position.equals("D")) {
                    player.position = Position.Verteidiger;
                } else if (position.equals("F") || position.equals("C") || position.equals("RW") || position.equals("LW")) {
                    player.position = Position.Stürmer;
                }
            } else if (i == 2) {
                // Name
                Element nameAndUrl = cell.select("a").first();
                if (nameAndUrl != null) {
                    String[] name = nameAndUrl.text().split(" ");
                    player.lastName = name[1];
                    player.firstName = name[0];
                    player.link = "https://www.del-2.org/" + nameAndUrl.attr("href");
                }
            } else if (i == 3) {
                // Shoot hand
                if (cell.text().equals("L")) {
                    player.hand = "Links";
                } else {
                    player.hand = "Rechts";
                }
            } else if (i == 4) {
                // Nation
                String nation = cell.text();
                player.nation = NationUtils.getNation(nation);
                player.nationImgUrl = NationUtils.getNationImgUrl(player.nation);
            } else if (i == 5) {
                // Size
                String size = cell.text();
                try {
                    player.size = Integer.parseInt(size.replaceAll("[\\D]", ""));
                } catch (NumberFormatException e) {
                }
            } else if (i == 6) {
                // Weight
                String weight = cell.text();
                try {
                    player.weight = Integer.parseInt(weight.replaceAll("[\\D]", ""));
                } catch (NumberFormatException e) {
                }
            } else if (i == 7) {
                // Birthday
                String birthday = cell.text();
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                try {
                    player.birthday = sdf.parse(birthday);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        player = getPlayerStats(player);
        return player;
    }

    private static Player getPlayerStats(Player player) {
        if (player == null) {
            return new Player();
        }
        Document detailedPlayerPage = getDocument(player.link);
        if (detailedPlayerPage == null) {
            return player;
        }
        Elements playerPotrait = detailedPlayerPage.getElementsByClass("spielerportrait");
        String playerPhoto = playerPotrait.select(".spielerbild").attr("src");
        if (playerPhoto.equals("https://www.holema.de/assets/dbimages/no_image_200_0.png")) {
            player.imageUrl = "";
        } else {
            player.imageUrl = playerPhoto;
        }
        Elements playerSeasonHistoryTable = detailedPlayerPage.select("#tabelle").select("tbody").select("tr");
        if (playerSeasonHistoryTable.size() > 0) {
            int currentSeason = playerSeasonHistoryTable.size() - 1;
            Element currentSeasonStatsRow = playerSeasonHistoryTable.get(currentSeason);
            Elements cells = currentSeasonStatsRow.select("td");
            for (int i = 0; i < cells.size(); i++) {
                Element cell = cells.get(i);
                if (i == GAMES) {
                    int gamesPlayed = Integer.valueOf(cell.text());
                    player.gamesPlayed = gamesPlayed;
                } else if (i == GOALS) {
                    int goals = Integer.valueOf(cell.text());
                    player.goals = goals;
                } else if (i == ASSISTS) {
                    int assists = Integer.valueOf(cell.text());
                    player.assists = assists;
                } else if (i == SCORER_POINTS) {
                    int scorerPoints = Integer.valueOf(cell.text());
                    player.scorerpoints = scorerPoints;
                } else if (i == PENALTY_MINUTES) {
                    int penaltyMinutes = Integer.valueOf(cell.text());
                    player.penaltyMinutes = penaltyMinutes;
                } else if (i == PLUS_MINUS) {
                    int plusMinus = Integer.valueOf(cell.text());
                    player.plusMinus = plusMinus;
                }
            }
        }
        return player;
    }

    public static Document getDocument(String strlUrl) {
        try {
            return getDel2Document(strlUrl);
        } catch (IOException e) {
            System.out.println("Error: " + strlUrl);
            try {
                System.out.println("With Proxy: " + AppConfiguration.read().getProxyUrl() + ":" + AppConfiguration.read().getProxyPort());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            //Download new Proxy and try again
            ProxyCrawler.updateProxy();
            try {
                return getDel2Document(strlUrl);
            } catch (IOException e1) {
                e1.printStackTrace();
                return null;
            }
        }
    }

    private static Document getDel2Document(String strUrl) throws IOException {
        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {

                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        //No need to implement.
                    }

                    public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        //No need to implement.
                    }
                }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            System.out.println(e);
        }

        AppConfiguration config = AppConfiguration.read();
        URL url = new URL(strUrl);
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(config.getProxyUrl(), config.getProxyPort()));
        HttpURLConnection uc = (HttpURLConnection) url.openConnection();
        uc.setConnectTimeout(60000);
        uc.setReadTimeout(60000);
        uc.setRequestProperty("User-Agent",
                "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19");

        try {
            uc.connect();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return getDel2Document(strUrl);
        }

        final int responseCode = uc.getResponseCode();
        if (responseCode >= 400) {
            System.out.println("Wrong Proxy, update.");
            ProxyCrawler.updateProxy();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return getDel2Document(strUrl);
        }
        String line;
        StringBuffer tmp = new StringBuffer();
        InputStream inputStream = uc.getInputStream();
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        while ((line = in.readLine()) != null) {
            tmp.append(line);
        }
        inputStream.close();
        uc.disconnect();

        return Jsoup.parse(String.valueOf(tmp));
    }
}
