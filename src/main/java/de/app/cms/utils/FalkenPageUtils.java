package de.app.cms.utils;


import de.app.cms.model.news.News;
import de.app.cms.model.news.NewsType;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.Calendar;

public class FalkenPageUtils {

    public static News getNewsForLink(String link){
        Document document = Utils.get(link);

        final Element article = document.select("article").first();
        final String title = article.getElementsByClass("entry-title").text();
        final String imgUrl = article.select("img").first().attr("src");
        String description = article.getElementsByClass("entry-content").text();

        News toSaveNews = new News();
        toSaveNews.setNewsType(NewsType.Web);
        toSaveNews.setDate(Calendar.getInstance().getTime());
        toSaveNews.setTitle(title);
        toSaveNews.setImgUrl(imgUrl);
        toSaveNews.setLink(link);

        int maxLength = 150;
        if (Math.min(description.length(), maxLength) == maxLength) {
            description = description.substring(0, Math.min(description.length(), maxLength)) + "...";
        }
        toSaveNews.setSecondaryTitle(description);

        return toSaveNews;
    }
}

