package de.app.cms.utils;

public class NameUtils {

    /**
     * Gibt den richtigen Namen des Teams zurueck
     * @param teamName
     * @return
     */
    public static String getTeamName(String teamName) {
        if(teamName.equals("SC Bietigheim Steelers")){
            return "Bietigheim Steelers";
        } else if(teamName.equals("Rote Teufel Bad Nauheim")){
            return "EC Bad Nauheim";
        }
        return teamName;
    }

    public static String shortFormOfTeam(String name){
        if(name.equals("Heilbronner Falken")){
            return "HEC";
        } else if(name.equals("Bietigheim Steelers")){
            return "SCB";
        } else if(name.equals("Hannover Indians")){
            return "ECH";
        } else if(name.equals("EVL Landshut Eishockey")){
            return "EVL";
        } else if(name.equals("SC Riessersee")){
            return "SCR";
        } else if(name.equals("Starbulls Rosenheim")){
            return "SBR";
        } else if(name.equals("ESV Kaufbeuren")){
            return "ESVK";
        } else if(name.equals("Fischtown Pinguins Bremerhaven")){
            return "REVB";
        } else if(name.equals("Fischtown Pinguins")){
            return "REVB";
        }else if(name.equals("Lausitzer Füchse")){
            return "EHCL";
        } else if(name.equals("Ravensburg Towerstars")){
            return "EVR";
        } else if(name.equals("Eispiraten Crimmitschau")){
            return "ETC";
        } else if(name.equals("Dresdner Eislöwen")){
            return "ESCD";
        } else if(name.equals("Schwenninger Wild Wings")){
            return "SWW";
        } else if(name.equals("Etoile Noire de Strasbourg")){
            return "ENS";
        } else if(name.equals("EC Bad Nauheim")){
            return "ECBN";
        } else if(name.equals("Füchse Duisburg")){
            return "EVD";
        } else if(name.equals("Adler Mannheim")){
            return "MERC";
        } else if(name.equals("Löwen Frankfurt")){
            return "LFEV";
        } else if(name.equals("EHC Bayreuth Tigers")){
            return "EHCB";
        } else if(name.equals("EHC Wölfe Freiburg")){
            return "EHCF";
        } else if(name.equals("Kassel Huskies")){
            return "EJK";
        } else if(name.equals("EC Bad Tölz")){
            return "ECT";
        } else if(name.equals("Adler Mannheim")){
            return "MERC";
        } else if(name.startsWith("SC Langenthal")){
            return "SCL";
        } else if(name.startsWith("EC Kassel Huskies")){
            return "EJK";
        } else {
            return "";
        }
    }

    public static String getTeamNameForUrl(String url) {
        if (url.contains("esv_kaufbeuren")) {
            return "ESV Kaufbeuren";
        } 
        return null;
    }

    public static String tickerToDel2Mapper(String falkenTickerName) {
        switch (falkenTickerName) {
            case "ETC Crimmitschau":
                return "Eispiraten Crimmitschau";
            case "Fischtown Pinguins":
                return "Pinguins Bremerhaven";
            case "Kassel Huskies":
                return "EC Kassel Huskies";
            case "Bad Nauheim":
                return "EC Bad Nauheim";
            case "SC Bietigheim Steelers":
                return "Bietigheim Steelers";
            case "Wölfe Freiburg":
                return "EHC Freiburg";
            default:
                return falkenTickerName;
        }
    }
}
