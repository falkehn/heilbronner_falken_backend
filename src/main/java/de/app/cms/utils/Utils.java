package de.app.cms.utils;

import de.app.cms.config.CMSConfig;
import de.app.cms.model.player.Player;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

/**
 * This Class is Part of the Project cms Copyright: Lidl E-Commerce
 * GmbH & Co. KG Stiftsbergstr. 1 74172 Neckarsulm
 *
 * @author tomaszadamczyk
 */
public class Utils {

    public String getHtmlSkeleton(String path) {
        try {
            InputStream in = getClass().getResourceAsStream("/skeletons/" + path);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void savePlayer(Player player, String fileName){
        try {
            FileOutputStream fos = new FileOutputStream(CMSConfig.getInstance().get(CMSConfig.TEAM_PATH) + "/" + fileName);
            fos.write(HtmlBuilder.getPlayerHtml(player).getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String toMD5(String input) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(input.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            String hashtext = bigInt.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String shortenUrl(String longUrl) {
        if (longUrl == null) {
            return longUrl;
        }

        try {
            URL url = new URL("http://goo.gl/api/url");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("User-Agent", "toolbar");

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write("url=" + URLEncoder.encode(longUrl, "UTF-8"));
            writer.close();

            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line + '\n');
            }
            String json = sb.toString();
            return json.substring(json.indexOf("http"), json.indexOf("\"", json.indexOf("http")));
        } catch (MalformedURLException e) {
            return longUrl;
        } catch (IOException e) {
            return longUrl;
        }
    }

    public static void saveFile(String path, byte[] bytes) throws IOException {
        FileOutputStream fos = new FileOutputStream(path);
        fos.write(bytes);
        fos.close();
    }

    public static void sendMail(String html) {
        sendMail(html, "Proxy geht nicht mehr");
    }

    public static void sendMail(String html, String subject) {
        final String username = "skatespotshunter@gmail.com";
        final String password = "hd62e+#c";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("tadamczyk.javajoe@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("dev.sieber@gmail.com"));
            message.setSubject(subject);
            message.setContent(html, "text/html; charset=utf-8");

            Transport.send(message);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (javax.mail.MessagingException e) {
            e.printStackTrace();
        }
    }

    public static Document get(String url) {
        try {
            return Jsoup.connect(url)
                    .userAgent("Mozilla")
                    .header("Accept", "text/html")
                    .header("Accept-Encoding", "gzip,deflate")
                    .header("Accept-Language", "it-IT,en;q=0.8,en-US;q=0.6,de;q=0.4,it;q=0.2,es;q=0.2")
                    .header("Connection", "keep-alive")
                    .ignoreContentType(true)
                    .referrer("http://www.google.com")
                    .timeout(60000)
                    .validateTLSCertificates(false)
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
