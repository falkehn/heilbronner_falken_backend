package de.app.cms.observer;

import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.Post;
import de.app.cms.config.CMSConfig;
import de.app.cms.model.image.BannerImage;
import de.app.cms.model.news.News;
import de.app.cms.model.news.NewsType;
import de.app.cms.repo.BannerImageRepository;
import de.app.cms.repo.NewsRepository;
import de.app.cms.utils.HtmlBuilder;
import de.app.cms.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static de.app.cms.utils.HtmlBuilder.*;
import static de.app.cms.utils.HtmlBuilder.ALIGN_LEFT;
import static de.app.cms.utils.HtmlBuilder.FONT_NORMAL;

@Component
@EnableScheduling
public class FacebookObserver {

    @Autowired
    private NewsRepository mNewsRepository;

    @Autowired
    private BannerImageRepository mBannerRepository;

    //15 min
    @Scheduled(fixedDelay = 900000)
    public void observeFacebook() {
        FacebookClient facebookClient = new DefaultFacebookClient();
        final FacebookClient.AccessToken accessToken = facebookClient.obtainAppAccessToken("981338925282240",
                "9da8d8265b98508d4ebc0ea48c619022");
        facebookClient = new DefaultFacebookClient(accessToken.getAccessToken());
        final Connection<Post> falkenFeed = facebookClient.fetchConnection("falken.heilbronn/feed", Post.class);
        for (int i=0; i<falkenFeed.getData().size(); i++) {
            final String id = falkenFeed.getData().get(i).getId();
            final Post post = facebookClient.fetchObject(id, Post.class,
                    Parameter.with("fields", "picture,full_picture,id,message"));
            if (post.getMessage() == null && mBannerRepository.findOne(id) == null) {
                // banner
                final BannerImage bannerImage = new BannerImage();
                bannerImage.id = id;
                bannerImage.url = post.getFullPicture();
                mBannerRepository.save(bannerImage);

                //send mail
                Utils.sendMail(HtmlBuilder.getMailHtml(bannerImage), "Neues Falken-Bild");
            } else if (mNewsRepository.findByCustomId(id) == null) {
                // post
                final String title = getTitle(post.getMessage());
                News news = new News();
                news.setCustomId(post.getId());
                news.setTitle(title);
                news.setDate(Calendar.getInstance().getTime());
                news.setNewsType(NewsType.Web);
                // to get id
                news = mNewsRepository.save(news);
                final String fileName = news.getId() + ".html";
                final String filePath = CMSConfig.getInstance().get(CMSConfig.NEWS_HTML_PATH) + "/" + fileName;

                final String message = getMessage(title, post.getMessage());
                final String htmlPage = generateHtml(title, message, post.getFullPicture());
                try {
                    Utils.saveFile(filePath, htmlPage.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final String link = Utils.shortenUrl("http://falkenapp.de/storage/news/" + fileName);
                news.setLink(link);
                news.setImgUrl(post.getFullPicture());
                news.setSecondaryTitle(getSecondaryTitle(message));

                if (mNewsRepository.save(news) != null) {
                    //send mail
                    Utils.sendMail(HtmlBuilder.getMailHtml(news, post.getLink()), "Neue Falken-News");
                }
            }
        }
    }

    private String getTitle(String message) {
        if (message != null) {
            final int startIndex = message.indexOf("\n\n");
            if (startIndex > 0 && startIndex < message.length()) {
                String remove = message.substring(startIndex, message.length());
                String title = message.replace(remove, "");
                title = title.replace(":", "");
                return title;
            }
        }
        return "Falken News";
    }

    private String getMessage(String title, String message) {
        if (message == null) {
            return null;
        }
        if (message.startsWith(title)) {
            message = message.substring(title.length(), message.length());
        }
        if (message.startsWith(":")) {
            message = message.substring(1, message.length());
        }
        message = message.replace("\n\n", "");
        return message;
    }

    private String getSecondaryTitle(String message) {
        final int maxLength = 150;
        if (Math.min(message.length(), maxLength) == maxLength) {
            message = message.substring(0, Math.min(message.length(), maxLength)) + "...";
        }
        return message;
    }

    private String generateHtml(String title, String message, String imageLink) {
        SimpleDateFormat htmlSdf = new SimpleDateFormat("EEEE, dd.MM.yyyy HH:mm");
        String date = htmlSdf.format(Calendar.getInstance().getTime());

        HtmlBuilder htmlBuilder = new HtmlBuilder();
        htmlBuilder.reset();

        if (imageLink != null && imageLink.length() > 0) {
            htmlBuilder.addImage(imageLink);
        }
        htmlBuilder.addText(date + " Uhr", COLOR_BLUE, FONT_NORMAL, 18, ALIGN_LEFT);
        htmlBuilder.addSpace();
        htmlBuilder.addText(title, COLOR_RED, FONT_BOLD, 20, ALIGN_LEFT);
        htmlBuilder.addSpace();
        htmlBuilder.addText(message, COLOR_BLACK, FONT_NORMAL, 16, ALIGN_LEFT);

        Utils utils = new Utils();
        final String htmlSkeleton = utils.getHtmlSkeleton("news_skeleton.html");
        return htmlSkeleton.replace("INSERT_HTML_HERE", htmlBuilder.renderBody());
    }
}
