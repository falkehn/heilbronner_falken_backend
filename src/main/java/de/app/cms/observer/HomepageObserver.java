package de.app.cms.observer;

import de.app.cms.model.news.News;
import de.app.cms.model.news.NewsType;
import de.app.cms.push.PushMessageSender;
import de.app.cms.repo.NewsRepository;
import de.app.cms.utils.FalkenPageUtils;
import de.app.cms.utils.Utils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class HomepageObserver {

    @Autowired
    private NewsRepository mNewsRepository;

    @Autowired
    private PushMessageSender mPushMessageSender;

    //15 min
    @Scheduled(fixedDelay = 900000)
    public void observerHomepage() throws Exception {
        final String homepageNews = "https://www.heilbronner-falken.de/aktuelles/";
        final Document page = Utils.get(homepageNews);
        Elements article = page.select("article");
        for (Element a : article) {
            final String url = a.select("a").first().attr("href");
            if (mNewsRepository.findByCustomId(url) == null) {
                final News falkennews = FalkenPageUtils.getNewsForLink(url);
                falkennews.setCustomId(url);
                falkennews.setPublic(true);
                boolean push = true;
                if (mNewsRepository.save(falkennews) != null && push) {
                    mPushMessageSender.sendPush(
                            falkennews.getTitle(),
                            falkennews.getSecondaryTitle(),
                            String.valueOf(falkennews.getId()),
                            NewsType.Web,
                            falkennews.getLink()
                    );
                }
            }
        }
    }

}
